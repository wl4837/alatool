package io.gitee.wl4837.alatool.core.lang;

public class JsNull implements JsVar, java.io.Serializable, Comparable<JsBoolean> {

    public static final JsNull NULL = new JsNull();

    private Object value;

    public JsNull() {
        Object value = null;
    }

    public Object getValue() {
        return this.value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public int compareTo(JsBoolean o) {
        return 0;
    }

    @Override
    public String toString() {
        return "null";
    }

    @Override
    public boolean equals(Object obj) {
        if (null == obj) {
            return false;
        }
        return JsNull.class.equals(obj.getClass());
    }

}
