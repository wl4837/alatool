package io.gitee.wl4837.alatool.core.response;

/**
 * @author wangliang
 */
public class ResponseSuccessException extends ResponseException {

    public ResponseSuccessException(String message) {
        super(message);
        this.code = 200;
    }

    public ResponseSuccessException(String message, Throwable e) {
        super(message, e);
        this.code = 200;
    }

    public ResponseSuccessException(Integer code, String message) {
        super(code, message);
    }

    public ResponseSuccessException(Integer code, String message, Throwable e) {
        super(code, message, e);
    }

}
