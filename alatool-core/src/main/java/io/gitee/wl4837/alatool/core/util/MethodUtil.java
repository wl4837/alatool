package io.gitee.wl4837.alatool.core.util;


import cn.hutool.core.util.StrUtil;
import io.gitee.wl4837.alatool.core.lang.MethodType;

/**
 * @author kingood
 */
public class MethodUtil {

    /**
     * 获取序列号后的方法参数
     *
     * @param methodTypeStr 序列号字段
     * @return 方法参数
     */
    public static MethodType getMethodType(String methodTypeStr) {
        String paramStr = methodTypeStr.substring(methodTypeStr.indexOf("(") + 1, methodTypeStr.lastIndexOf(")"));
        String returnStr = methodTypeStr.substring(methodTypeStr.lastIndexOf(")") + 1);
        try {
            MethodType methodType = new MethodType();
            if (StrUtil.isNotEmpty(paramStr)) {
                String[] splits = paramStr.split(";");
                Class<?>[] parameterTypes = new Class<?>[splits.length];
                for (int i = 0; i < splits.length; i++) {
                    String parameterType = splits[i].substring(1).replaceAll("/", ".").replace(";", "");
                    parameterTypes[i] = Class.forName(parameterType);
                }
                methodType.setParameterTypes(parameterTypes);
            }
            if (StrUtil.isNotEmpty(returnStr) && !StrUtil.equals(returnStr, "V")) {
                returnStr = returnStr.substring(1).replaceAll("/", ".").replace(";", "");
                methodType.setReturnType(Class.forName(returnStr));
            }
            return methodType;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
