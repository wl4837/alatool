package io.gitee.wl4837.alatool.core.lang;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public final class JsArray implements JsVar, java.io.Serializable, Comparable<JsArray> {

    private static final long serialVersionUID = 1L;

    private final List<JsVar> value;

    @SuppressWarnings("all")
    public static final Class<List> TYPE = List.class;

    public JsArray() {
        this.value = new LinkedList<>();
    }

    public JsArray(List<JsVar> value) {
        this.value = value;
    }

    public JsArray(JsVar ...value) {
        this.value = new LinkedList<>();
        this.value.addAll(Arrays.asList(value));
    }

    @Override
    public int compareTo(JsArray o) {
        return 0;
    }

    public void push(JsVar jsVar) {
        value.add(jsVar);
    }

    public void pop() {

    }

    public String toString() {
        return this.value.toString();
    }

}
