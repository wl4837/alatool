package io.gitee.wl4837.alatool.core.lang;

import java.util.HashMap;
import java.util.Map;

public class JsObject implements JsVar, java.io.Serializable, Comparable<JsObject> {

    @SuppressWarnings("all")
    public static final Class<Object> TYPE = Object.class;

    private Map<String, JsVar> value;

    public JsObject() {
        this.value = new HashMap<>();
    }

    @Override
    public int compareTo(JsObject o) {
        return 0;
    }

}
