package io.gitee.wl4837.alatool.core.lang;


public final class JsNumber implements JsVar, Comparable<JsNumber> {

    @SuppressWarnings("all")
    public static final Class<Integer> TYPE = Integer.class;

    private Integer value;

    public JsNumber(int original) {
        this.value = original;
    }

    public JsNumber(Integer original) {
        this.value = original;
    }

    public JsNumber(String original) {
        this.value = Integer.valueOf(original);
    }

    @Override
    public int compareTo(JsNumber o) {
        return this.value.compareTo(o.value);
    }

    public String toString() {
        return this.value.toString();
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public static JsNumber valueOf(JsNumber value) {
        return new JsNumber(value.value);
    }

    public static JsNumber valueOf(String value) {
        return new JsNumber(value);
    }

    public static JsNumber valueOf(Integer value) {
        return new JsNumber(value);
    }

    public static JsNumber valueOf(int value) {
        return new JsNumber(value);
    }

}
