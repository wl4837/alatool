package io.gitee.wl4837.alatool.core.validate;

import java.util.List;

/**
 * @author wangliang
 */
public interface FormValidateResult {

    int getCode();

    void setCode(int code);

    String getMessage();

    void setMessage(String message);

}
