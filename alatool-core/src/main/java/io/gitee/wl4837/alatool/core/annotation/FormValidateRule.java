package io.gitee.wl4837.alatool.core.annotation;

import io.gitee.wl4837.alatool.core.validate.FormValidator;
import io.gitee.wl4837.alatool.core.validate.validator.DefaultValidator;

import java.lang.annotation.*;

/**
 * 表单验证规则
 *
 * @author wangliang
 */
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface FormValidateRule {

    /**
     * @return 是否必填验证
     */
    boolean required() default false;

    /**
     * @return 错误返回消息
     */
    String message() default "请输入值";

    /**
     * @return 验证最小值
     */
    int min() default 0;

    /**
     * @return 验证最大值
     */
    int mix() default 0;

    /**
     * @return 自定义验证类
     */
    Class<? extends FormValidator> validator() default DefaultValidator.class;

    /**
     * @return 预期结果
     */
    String[] resultStrings() default {};

    /**
     * @return 预期结果
     */
    int[] resultIntegers() default {};

    /**
     * @return 预期结果
     */
    long[] resultLongs() default {};

    /**
     * @return 预期结果
     */
    boolean[] resultBooleans() default {};

    /**
     * @return 预期结果
     */
    double[] resultDoubles() default {};

    /**
     * @return 预期结果
     */
    float[] resultFloats() default {};

}

