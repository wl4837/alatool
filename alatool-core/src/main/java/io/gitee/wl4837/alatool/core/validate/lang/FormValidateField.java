package io.gitee.wl4837.alatool.core.validate.lang;

import java.lang.reflect.Field;

/**
 * @author wangliang
 */
public class FormValidateField<T> extends FormValidateObject<T> {

    private Field field;

    public FormValidateField(T value, Field field) {
        super(value);
        this.field = field;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

}
