package io.gitee.wl4837.alatool.core.util;

import io.gitee.wl4837.alatool.core.getter.MethodBaseFieldSetter;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * 设置封包数据操作类
 *
 * @author kingood
 */
public class SetterUtil {

    /**
     * 设置字段值
     *
     * @param object 赋值对象
     * @param methodFieldSetter 赋值的方法
     * @param value 设置值
     * @param <T> 对象类型
     * @param <U> 值类型
     */
    public static <T, U> void setValue(T object, MethodBaseFieldSetter<T, U> methodFieldSetter, U value) {
        try {
            Method method = methodFieldSetter.getMethod();
            if (value == null) {
                method.invoke(object, new Object[]{null});
            } else {
                method.invoke(object, value);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 字段为空则赋默认值
     *
     * @param object 赋值对象
     * @param methodFieldSetter 赋值的方法
     * @param value 设置值
     * @param <T> 对象类型
     * @param <U> 值类型
     */
    public static <T, U> void setNullDefault(T object, MethodBaseFieldSetter<T, U> methodFieldSetter, U value) {
        try {
            Field field = methodFieldSetter.getField();
            Object o = field.get(object);
            if (ObjectUtil.isNull(o)) {
                setValue(object, methodFieldSetter, value);
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取实例属性名称
     *
     * @param methodFieldSetter 对象字段
     * @return 字段名称
     * @param <P> 对象类型
     * @param <R> 值类型
     */
    public static <P, R> String getFieldName(MethodBaseFieldSetter<P, R> methodFieldSetter) {
        try {
            return methodFieldSetter.getFieldName();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
