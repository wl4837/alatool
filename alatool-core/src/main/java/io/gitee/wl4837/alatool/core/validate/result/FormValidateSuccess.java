package io.gitee.wl4837.alatool.core.validate.result;

import io.gitee.wl4837.alatool.core.validate.FormValidateResult;

/**
 * @author wangliang
 */
public class FormValidateSuccess implements FormValidateResult {

    private int code = 200;

    private String message;

    public FormValidateSuccess() {

    }

    public FormValidateSuccess(String message) {
        this.message = message;
    }

    public FormValidateSuccess(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

}
