package io.gitee.wl4837.alatool.core.validate;

/**
 * @author wangliang
 */
public class FormValidateData<T> {

    private T data;

    public FormValidateData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
