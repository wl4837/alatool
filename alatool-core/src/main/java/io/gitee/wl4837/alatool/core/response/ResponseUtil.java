package io.gitee.wl4837.alatool.core.response;

import io.gitee.wl4837.alatool.core.lang.HttpStatusCode;

import java.util.Objects;

/**
 * 响应信息主体
 *
 * @author wangliang
 */
public class ResponseUtil {

    /**
     * 如果错误则投递抛出
     * @param r 返回值
     * @return 抛出异常
     * @param <T> 返回值类型
     */
    public static <T> R<T> throwError(R<T> r) {
        if (!Objects.equals(HttpStatusCode.OK.getCode(), r.getCode())) {
            throw new ResponseException(r.getMessage());
        }
        return r;
    }

}
