package io.gitee.wl4837.alatool.core.validate.result;

import io.gitee.wl4837.alatool.core.validate.FormValidateResult;

/**
 * @author wangliang
 */
public class FormValidateError implements FormValidateResult {

    private int code = 400;

    private String message;

    public FormValidateError() {

    }

    public FormValidateError(String message) {
        this.message = message;
    }

    public FormValidateError(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

}
