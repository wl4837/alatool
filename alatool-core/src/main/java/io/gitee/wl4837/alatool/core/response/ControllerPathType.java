package io.gitee.wl4837.alatool.core.response;

/**
 * @author wangliang
 */

public enum ControllerPathType {

    DELETE("/delete", "删除"),
    CREATE("/create", "创建"),
    UPDATE("/update", "更新"),
    SAVE("/save", "保存"),
    LIST("/list", "列表"),
    DETAIL("/detail", "详情");

    private String path;

    private String description;

    ControllerPathType(String path, String description) {
        this.path = path;
        this.description = description;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String type) {
        this.path = path;
    }

    public String getName() {
        return name();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
