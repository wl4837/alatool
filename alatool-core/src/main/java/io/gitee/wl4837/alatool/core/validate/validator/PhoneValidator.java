package io.gitee.wl4837.alatool.core.validate.validator;

import io.gitee.wl4837.alatool.core.util.PhoneUtil;
import io.gitee.wl4837.alatool.core.validate.*;
import io.gitee.wl4837.alatool.core.validate.result.FormValidateError;
import io.gitee.wl4837.alatool.core.validate.result.FormValidateSuccess;

public class PhoneValidator implements FormValidator {


    @Override
    public FormValidateResult handle(Object data, FormValidateRuleData formValidateRuleData) {
        if (null == data) {
            if (formValidateRuleData.getRequired()) {
                return new FormValidateError(formValidateRuleData.getMessage());
            } else {
                return new FormValidateSuccess();
            }
        }
        String phone = data.toString();
        if ("".equals(phone)) {
            if (formValidateRuleData.getRequired()) {
                return new FormValidateError(formValidateRuleData.getMessage());
            } else {
                return new FormValidateSuccess();
            }
        }
        if (PhoneUtil.isPhone(phone)) {
            return new FormValidateSuccess();
        } else {
            return new FormValidateError(formValidateRuleData.getMessage());
        }
    }

}
