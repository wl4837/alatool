package io.gitee.wl4837.alatool.core.lang;

public final class JsBoolean implements JsVar, java.io.Serializable, Comparable<JsBoolean> {

    @SuppressWarnings("all")
    public static final Class<Boolean> TYPE = Boolean.class;

    private Boolean value;

    public static final JsBoolean TRUE = new JsBoolean(true);

    public static final JsBoolean FALSE = new JsBoolean(false);

    public JsBoolean(boolean original) {
        this.value = original;
    }

    public JsBoolean(Boolean original) {
        this.value = original;
    }

    public Boolean getValue() {
        return this.value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public void setValue(Boolean value) {
        this.value = value;
    }

    @Override
    public int compareTo(JsBoolean o) {
        return 0;
    }

    public String toString() {
        return this.value ? "true" : "false";
    }

    public boolean booleanValue() {
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        if (null == obj) {
            return false;
        }
        if (JsBoolean.class.equals(obj.getClass())) {
            return this.value == ((JsBoolean) obj).booleanValue();
        } else if (Boolean.class.equals(obj.getClass())) {
            return this.value == ((Boolean) obj).booleanValue();
        } else {
            return false;
        }
    }

}
