
package io.gitee.wl4837.alatool.core.util;

import cn.hutool.core.collection.ListUtil;
import io.gitee.wl4837.alatool.core.annotation.FormValidateRule;
import io.gitee.wl4837.alatool.core.annotation.FormValidateRules;
import io.gitee.wl4837.alatool.core.getter.MethodBaseFieldGetter;
import io.gitee.wl4837.alatool.core.validate.*;
import io.gitee.wl4837.alatool.core.validate.exception.FormValidateResultException;
import io.gitee.wl4837.alatool.core.validate.lang.*;
import io.gitee.wl4837.alatool.core.validate.FormValidateItem;
import io.gitee.wl4837.alatool.core.validate.result.FormValidateError;
import io.gitee.wl4837.alatool.core.validate.result.FormValidateSuccess;

import java.lang.reflect.Field;
import java.util.*;

/**
 * 表单校验工具
 *
 * @author wangliang
 */
public class FormValidateUtil {

    /**
     * 校验表单对象
     *
     * @param data 数据
     * @param <T>  对象类型
     * @return 对象类型
     */
    @SuppressWarnings("all")
    public static <T> FormValidateObject check(T data) {
        return check(data, FormValidateRequiredType.NONE);
    }

    /**
     * 校验表单对象
     *
     * @param list 数据
     * @param <T>  对象类型
     * @return 对象类型
     */
    @SuppressWarnings("all")
    public static <T> FormValidateList check(List<T> list) {
        return check(list, FormValidateRequiredType.NONE);
    }

    /**
     * 校验表单对象
     *
     * @param map 数据
     * @param <K> 对象键
     * @param <T> 对象类型
     * @return 对象类型
     */
    @SuppressWarnings("all")
    public static <K, T> FormValidateMap<T> check(Map<K, T> map) {
        return check(map, FormValidateRequiredType.NONE);
    }

    /**
     * 校验表单对象
     *
     * @param data         数据
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     * @return 对象类型
     */
    @SuppressWarnings("all")
    public static <T> FormValidateObject check(T data, FormValidateRequiredType requiredType) {
        if (null == data) {
            throw new FormValidateResultException("The form validation object is empty");
        } else {
            Class<T> clz = (Class<T>) data.getClass();
            return check(data, clz, requiredType);
        }
    }

    /**
     * 校验表单对象
     *
     * @param list         数据
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     * @return 对象类型
     */
    public static <T> FormValidateList check(List<T> list, FormValidateRequiredType requiredType) {
        FormValidateList formValidateList = new FormValidateList(list);
        for (T item : list) {
            formValidateList.getItems().add(check(item, requiredType));
        }
        return formValidateList;
    }

    /**
     * 校验表单对象
     *
     * @param map          数据
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     * @param <K>          对象键
     * @return 对象类型
     */
    public static <K, T> FormValidateMap check(Map<K, T> map, FormValidateRequiredType requiredType) {
        FormValidateMap formsValidateMap = new FormValidateMap(map);
        for (Map.Entry<K, T> item : map.entrySet()) {
            formsValidateMap.getItems().add(check(item.getValue(), requiredType));
        }
        return formsValidateMap;
    }

    /**
     * 校验表单对象
     *
     * @param data                   数据
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @return 对象类型
     */
    @SuppressWarnings("all")
    public static <T, R> FormValidateObject check(T data, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        return check(data, FormValidateRequiredType.NONE, methodBaseFieldGetters);
    }

    /**
     * 校验表单对象
     *
     * @param list                   数据
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @return 对象类型
     */
    @SafeVarargs
    @SuppressWarnings("all")
    public static <T, R> FormValidateList check(List<T> list, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        FormValidateList formValidateList = new FormValidateList(list);
        for (T item : list) {
            formValidateList.getItems().add(check(item, methodBaseFieldGetters));
        }
        return formValidateList;
    }

    /**
     * 校验表单对象
     *
     * @param map                    数据
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @param <K>                    对象键
     * @return 对象类型
     */
    @SafeVarargs
    public static <T, K, R> FormValidateMap check(Map<K, T> map, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        FormValidateMap formsValidateMap = new FormValidateMap(map);
        for (Map.Entry<K, T> item : map.entrySet()) {
            formsValidateMap.getItems().add(check(item.getValue(), methodBaseFieldGetters));
        }
        return formsValidateMap;
    }

    /**
     * 校验表单对象
     *
     * @param data                   数据
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @return 对象类型
     */
    @SuppressWarnings("all")
    public static <T, R> FormValidateObject check(T data, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        if (null == data) {
            throw new FormValidateResultException("The form validation object is empty");
        } else {
            Class<T> clz = (Class<T>) data.getClass();
            return check(data, clz, requiredType);
        }
    }

    /**
     * 校验表单对象
     *
     * @param list                   数据
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @return 对象类型
     */
    @SafeVarargs
    public static <T, R> FormValidateList check(List<T> list, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        FormValidateList formValidateList = new FormValidateList(list);
        for (T item : list) {
            formValidateList.getItems().add(check(item, requiredType, methodBaseFieldGetters));
        }
        return formValidateList;
    }

    /**
     * 校验表单对象
     *
     * @param map                    数据
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @param <K>                    对象键
     * @return 对象类型
     */
    @SafeVarargs
    public static <T, K, R> FormValidateMap check(Map<K, T> map, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        FormValidateMap formsValidateMap = new FormValidateMap(map);
        for (Map.Entry<K, T> item : map.entrySet()) {
            formsValidateMap.getItems().add(check(item.getValue(), requiredType, methodBaseFieldGetters));
        }
        return formsValidateMap;
    }

    /**
     * 校验表单对象
     *
     * @param data 数据
     * @param clz  数据类
     * @param <T>  对象类型
     * @return 校验类结果
     */
    @SuppressWarnings("all")
    public static <T> FormValidateClass check(T data, Class<T> clz) {
        return check(data, clz, FormValidateRequiredType.NONE);
    }

    /**
     * 校验表单对象
     *
     * @param list 数据
     * @param clz  数据类
     * @param <T>  对象类型
     * @return 校验类结果
     */
    @SuppressWarnings("all")
    public static <T> FormValidateList check(List<T> list, Class<T> clz) {
        return check(list, clz, FormValidateRequiredType.NONE);
    }

    /**
     * 校验表单对象
     *
     * @param map 数据
     * @param clz 数据类
     * @param <T> 对象类型
     * @param <K> 对象键
     * @return 校验类结果
     */
    @SuppressWarnings("all")
    public static <T, K> FormValidateMap check(Map<K, T> map, Class<T> clz) {
        return check(map, clz, FormValidateRequiredType.NONE);
    }

    /**
     * 校验表单对象
     *
     * @param data         数据
     * @param clz          数据类
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     * @return 校验类结果
     */
    @SuppressWarnings("all")
    public static <T> FormValidateClass check(T data, Class<T> clz, FormValidateRequiredType requiredType) {
        FormValidateClass formValidateClass = new FormValidateClass(data, clz);
        List<FormValidateRuleData> formValidateRuleDatas = new ArrayList<>();
        List<FormValidateRule> formValidateRules = new ArrayList<>();
        Class<?> next = clz;
        while (null != next && next != Object.class) {
            FormValidateRule formValidateRuleAnnotation = next.getAnnotation(FormValidateRule.class);
            FormValidateRules formValidateRulesAnnotation = next.getAnnotation(FormValidateRules.class);
            if (null != formValidateRuleAnnotation) {
                formValidateRules.add(formValidateRuleAnnotation);
            }
            if (null != formValidateRulesAnnotation) {
                formValidateRules.addAll(Arrays.asList(formValidateRulesAnnotation.value()));
            }
            next = next.getSuperclass();
        }
        for (FormValidateRule formValidateRule : formValidateRules) {
            FormValidateRuleData formValidateRuleData = new FormValidateRuleData();
            formValidateRuleData.setRequired(formValidateRule.required());
            formValidateRuleData.setValidator(formValidateRule.validator());
            formValidateRuleData.setMin(formValidateRule.min());
            formValidateRuleData.setMix(formValidateRule.mix());
            formValidateRuleData.setMessage(formValidateRule.message());
            formValidateRuleData.setResultBooleans(formValidateRule.resultBooleans());
            formValidateRuleData.setResultDoubles(formValidateRule.resultDoubles());
            formValidateRuleData.setResultFloats(formValidateRule.resultFloats());
            formValidateRuleData.setResultIntegers(formValidateRule.resultIntegers());
            formValidateRuleData.setResultStrings(formValidateRule.resultStrings());
            formValidateRuleData.setResultLongs(formValidateRule.resultLongs());
            formValidateRuleDatas.add(formValidateRuleData);
        }
        if (formValidateRuleDatas.size() > 0) {
            formValidateClass.setResults(check(data, requiredType, formValidateRuleDatas).getResults());
        }
        for (Field field : ClassUtil.getAllDeclaredFields(clz)) {
            try {
                field.setAccessible(true);
                formValidateClass.getItems().add(check(null == data ? null : field.get(data), field, requiredType));
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return formValidateClass;
    }

    /**
     * 校验表单对象
     *
     * @param list         数据
     * @param clz          数据类
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     * @return 校验类结果
     */
    @SuppressWarnings("all")
    public static <T> FormValidateList check(List<T> list, Class<T> clz, FormValidateRequiredType requiredType) {
        FormValidateList formValidateList = new FormValidateList(list);
        for (T item : list) {
            formValidateList.getItems().add(check(item, clz, requiredType));
        }
        return formValidateList;
    }

    /**
     * 校验表单对象
     *
     * @param map          数据
     * @param clz          数据类
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     * @param <K>          对象键
     * @return 校验类结果
     */
    @SuppressWarnings("all")
    public static <K, T> FormValidateMap check(Map<K, T> map, Class<T> clz, FormValidateRequiredType requiredType) {
        FormValidateMap formsValidateMap = new FormValidateMap(map);
        for (Map.Entry<K, T> item : map.entrySet()) {
            formsValidateMap.getItems().add(check(item.getValue(), clz, requiredType));
        }
        return formsValidateMap;
    }

    /**
     * 校验表单对象
     *
     * @param data                   数据
     * @param clz                    数据类
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @return 校验类结果
     */
    @SafeVarargs
    @SuppressWarnings("all")
    public static <T, R> FormValidateClass check(T data, Class<T> clz, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        return check(data, clz, FormValidateRequiredType.NONE, methodBaseFieldGetters);
    }

    /**
     * 校验表单对象
     *
     * @param list                   数据
     * @param clz                    数据类
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @return 校验类结果
     */
    @SafeVarargs
    @SuppressWarnings("all")
    public static <T, R> FormValidateList check(List<T> list, Class<T> clz, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        FormValidateList formValidateList = new FormValidateList(list);
        for (T item : list) {
            formValidateList.getItems().add(check(item, clz, methodBaseFieldGetters));
        }
        return formValidateList;
    }

    /**
     * 校验表单对象
     *
     * @param map                    数据
     * @param clz                    数据类
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @param <K>                    对象键
     * @return 校验类结果
     */
    @SafeVarargs
    @SuppressWarnings("all")
    public static <T, K, R> FormValidateMap check(Map<K, T> map, Class<T> clz, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        FormValidateMap formsValidateMap = new FormValidateMap(map);
        for (Map.Entry<K, T> item : map.entrySet()) {
            formsValidateMap.getItems().add(check(item.getValue(), clz, methodBaseFieldGetters));
        }
        return formsValidateMap;
    }

    /**
     * 校验表单对象
     *
     * @param data                   数据
     * @param clz                    数据类
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @return 校验类结果
     */
    @SafeVarargs
    @SuppressWarnings("all")
    public static <T, R> FormValidateClass check(T data, Class<T> clz, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        FormValidateClass formValidateClass = new FormValidateClass(data, clz);
        List<FormValidateRuleData> formValidateRuleDatas = new ArrayList<>();
        List<FormValidateRule> formValidateRules = new ArrayList<>();
        Class<?> next = clz;
        while (null != next && next != Object.class) {
            FormValidateRule formValidateRuleAnnotation = next.getAnnotation(FormValidateRule.class);
            FormValidateRules formValidateRulesAnnotation = next.getAnnotation(FormValidateRules.class);
            if (null != formValidateRuleAnnotation) {
                formValidateRules.add(formValidateRuleAnnotation);
            }
            if (null != formValidateRulesAnnotation) {
                formValidateRules.addAll(Arrays.asList(formValidateRulesAnnotation.value()));
            }
            next = next.getSuperclass();
        }
        for (FormValidateRule formValidateRule : formValidateRules) {
            FormValidateRuleData formValidateRuleData = new FormValidateRuleData();
            formValidateRuleData.setRequired(formValidateRule.required());
            formValidateRuleData.setValidator(formValidateRule.validator());
            formValidateRuleData.setMin(formValidateRule.min());
            formValidateRuleData.setMix(formValidateRule.mix());
            formValidateRuleData.setMessage(formValidateRule.message());
            formValidateRuleData.setResultBooleans(formValidateRule.resultBooleans());
            formValidateRuleData.setResultDoubles(formValidateRule.resultDoubles());
            formValidateRuleData.setResultFloats(formValidateRule.resultFloats());
            formValidateRuleData.setResultIntegers(formValidateRule.resultIntegers());
            formValidateRuleData.setResultStrings(formValidateRule.resultStrings());
            formValidateRuleData.setResultLongs(formValidateRule.resultLongs());
            formValidateRuleDatas.add(formValidateRuleData);
        }
        if (formValidateRuleDatas.size() > 0) {
            formValidateClass.setResults(check(data, requiredType, formValidateRuleDatas).getResults());
        }
        for (MethodBaseFieldGetter<T, R> methodBaseFieldGetter : methodBaseFieldGetters) {
            try {
                Field field = ClassUtil.getAllDeclaredField(clz, methodBaseFieldGetter);
                field.setAccessible(true);
                formValidateClass.getItems().add(check(null == data ? null : field.get(data), field, requiredType));
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return formValidateClass;
    }

    /**
     * 校验表单对象
     *
     * @param list                   数据
     * @param clz                    数据类
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @return 校验类结果
     */
    @SafeVarargs
    @SuppressWarnings("all")
    public static <T, R> FormValidateList check(List<T> list, Class<T> clz, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        FormValidateList formValidateList = new FormValidateList(list);
        for (T item : list) {
            formValidateList.getItems().add(check(item, clz, requiredType, methodBaseFieldGetters));
        }
        return formValidateList;
    }

    /**
     * 校验表单对象
     *
     * @param map                    数据
     * @param clz                    数据类
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @param <K>                    对象键
     * @return 校验类结果
     */
    @SafeVarargs
    @SuppressWarnings("all")
    public static <T, K, R> FormValidateMap check(Map<K, T> map, Class<T> clz, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        FormValidateMap formsValidateMap = new FormValidateMap(map);
        for (Map.Entry<K, T> item : map.entrySet()) {
            formsValidateMap.getItems().add(check(item.getValue(), clz, requiredType, methodBaseFieldGetters));
        }
        return formsValidateMap;
    }

    /**
     * 校验表单对象
     *
     * @param data         数据
     * @param field        属性
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     * @return 校验类结果
     */
    @SuppressWarnings("all")
    public static <T> FormValidateField check(T data, Field field, FormValidateRequiredType requiredType) {
        field.setAccessible(true);
        FormValidateField formValidateField = new FormValidateField(data, field);
        List<FormValidateRuleData> formValidateRuleDatas = new ArrayList<>();
        List<FormValidateRule> formValidateRules = new ArrayList<>();
        FormValidateRule formValidateRuleAnnotation = field.getAnnotation(FormValidateRule.class);
        FormValidateRules formValidateRulesAnnotation = field.getAnnotation(FormValidateRules.class);
        if (null != formValidateRuleAnnotation) {
            formValidateRules.add(formValidateRuleAnnotation);
        }
        if (null != formValidateRulesAnnotation) {
            formValidateRules.addAll(Arrays.asList(formValidateRulesAnnotation.value()));
        }
        for (FormValidateRule formValidateRule : formValidateRules) {
            FormValidateRuleData formValidateRuleData = new FormValidateRuleData();
            formValidateRuleData.setRequired(formValidateRule.required());
            formValidateRuleData.setValidator(formValidateRule.validator());
            formValidateRuleData.setMin(formValidateRule.min());
            formValidateRuleData.setMix(formValidateRule.mix());
            formValidateRuleData.setMessage(formValidateRule.message());
            formValidateRuleData.setResultBooleans(formValidateRule.resultBooleans());
            formValidateRuleData.setResultDoubles(formValidateRule.resultDoubles());
            formValidateRuleData.setResultFloats(formValidateRule.resultFloats());
            formValidateRuleData.setResultIntegers(formValidateRule.resultIntegers());
            formValidateRuleData.setResultStrings(formValidateRule.resultStrings());
            formValidateRuleData.setResultLongs(formValidateRule.resultLongs());
            formValidateRuleDatas.add(formValidateRuleData);
        }
        if (formValidateRuleDatas.size() > 0) {
            formValidateField.getResults().addAll(check(data, requiredType, formValidateRuleDatas).getResults());
        }
        return formValidateField;
    }

    /**
     * 校验表单对象
     *
     * @param data                 数据
     * @param formValidateRuleData 验证规则
     * @param <T>                  对象类型
     * @return 校验类结果
     */
    @SuppressWarnings("all")
    public static <T> FormValidateObject<T> check(T data, FormValidateRuleData formValidateRuleData) {
        return check(data, FormValidateRequiredType.NONE, formValidateRuleData);
    }

    /**
     * 校验表单对象
     *
     * @param data                 数据
     * @param requiredType         必填验证类型
     * @param formValidateRuleData 验证规则
     * @param <T>                  对象类型
     * @return 校验类结果
     */
    @SuppressWarnings("all")
    public static <T> FormValidateObject<T> check(T data, FormValidateRequiredType requiredType, FormValidateRuleData formValidateRuleData) {
        return check(data, requiredType, ListUtil.of(formValidateRuleData));
    }

    /**
     * 校验表单对象
     *
     * @param data                  数据
     * @param formValidateRuleDatas 验证规则组
     * @param <T>                   对象类型
     * @return 校验类结果
     */
    @SuppressWarnings("all")
    public static <T> FormValidateObject<T> check(T data, List<FormValidateRuleData> formValidateRuleDatas) {
        return check(data, FormValidateRequiredType.NONE, formValidateRuleDatas);
    }

    /**
     * 校验表单对象
     *
     * @param data                  数据
     * @param requiredType          必填验证类型
     * @param formValidateRuleDatas 验证规则组
     * @param <T>                   对象类型
     * @return 校验类结果
     */
    @SuppressWarnings("all")
    public static <T> FormValidateObject<T> check(T data, FormValidateRequiredType requiredType, List<FormValidateRuleData> formValidateRuleDatas) {
        FormValidateObject formValidateObject = new FormValidateObject(data);
        for (FormValidateRuleData formValidateRuleData : formValidateRuleDatas) {
            if (FormValidateRequiredType.REQUIRED == requiredType) {
                formValidateRuleData.setRequired(true);
            } else if (FormValidateRequiredType.OPTIONAL == requiredType) {
                formValidateRuleData.setRequired(false);
            } else {
                formValidateRuleData.setRequired(formValidateRuleData.getRequired());
            }
            try {
                formValidateObject.getResults().add(formValidateRuleData.getValidator().newInstance().handle(data, formValidateRuleData));
            } catch (InstantiationException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return formValidateObject;
    }

    /**
     * 校验表单对象
     *
     * @param data 数据
     * @param <T>  对象类型
     */
    public static <T> void checkThrowFirstError(T data) {
        throwFirstError(check(data));
    }

    /**
     * 校验表单对象
     *
     * @param data         数据
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     */
    public static <T> void checkThrowFirstError(T data, FormValidateRequiredType requiredType) {
        throwFirstError(check(data, requiredType));
    }

    /**
     * 校验表单对象
     *
     * @param data                   数据
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, R> void checkThrowFirstError(T data, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwFirstError(check(data, methodBaseFieldGetters));
    }

    /**
     * 校验表单对象
     *
     * @param data                   数据
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, R> void checkThrowFirstError(T data, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwAllError(check(data, requiredType, methodBaseFieldGetters));
    }

    /**
     * 校验表单对象
     *
     * @param list 数据
     * @param <T>  对象类型
     */
    public static <T> void checkThrowFirstError(List<T> list) {
        throwFirstError(check(list));
    }

    /**
     * 校验表单对象
     *
     * @param list         数据
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     */
    public static <T> void checkThrowFirstError(List<T> list, FormValidateRequiredType requiredType) {
        throwFirstError(check(list, requiredType));
    }

    /**
     * 校验表单对象
     *
     * @param list                   数据
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, R> void checkThrowFirstError(List<T> list, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwFirstError(check(list, methodBaseFieldGetters));
    }

    /**
     * 校验表单对象
     *
     * @param list                   数据
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, R> void checkThrowFirstError(List<T> list, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwAllError(check(list, requiredType, methodBaseFieldGetters));
    }

    /**
     * 校验表单对象
     *
     * @param map 数据
     * @param <T> 对象类型
     * @param <K> 对象键
     */
    public static <K, T> void checkThrowFirstError(Map<K, T> map) {
        throwFirstError(check(map));
    }

    /**
     * 校验表单对象
     *
     * @param map          数据
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     * @param <K>          对象键
     */
    public static <K, T> void checkThrowFirstError(Map<K, T> map, FormValidateRequiredType requiredType) {
        throwFirstError(check(map, requiredType));
    }

    /**
     * 校验表单对象
     *
     * @param map                    数据
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @param <K>                    对象键
     */
    @SafeVarargs
    public static <T, K, R> void checkThrowFirstError(Map<K, T> map, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwFirstError(check(map, methodBaseFieldGetters));
    }

    /**
     * 校验表单对象
     *
     * @param map                    数据
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @param <K>                    对象键
     */
    @SafeVarargs
    public static <T, K, R> void checkThrowFirstError(Map<K, T> map, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwAllError(check(map, requiredType, methodBaseFieldGetters));
    }

    /**
     * 校验表单对象
     *
     * @param data 数据
     * @param <T>  对象类型
     */
    public static <T> void checkThrowAllError(T data) {
        throwAllError(check(data));
    }

    /**
     * 校验表单对象
     *
     * @param data         数据
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     */
    public static <T> void checkThrowAllError(T data, FormValidateRequiredType requiredType) {
        throwAllError(check(data, requiredType));
    }

    /**
     * 校验表单对象
     *
     * @param data                   数据
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, R> void checkThrowAllError(T data, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwAllError(check(data, methodBaseFieldGetters));
    }

    /**
     * 校验表单对象
     *
     * @param data                   数据
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, R> void checkThrowAllError(T data, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwAllError(check(data, requiredType, methodBaseFieldGetters));
    }

    /**
     * 校验表单对象
     *
     * @param list 数据
     * @param <T>  对象类型
     */
    public static <T> void checkThrowAllError(List<T> list) {
        throwAllError(check(list));
    }

    /**
     * 校验表单对象
     *
     * @param list         数据
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     */
    public static <T> void checkThrowAllError(List<T> list, FormValidateRequiredType requiredType) {
        throwAllError(check(list, requiredType));
    }

    /**
     * 校验表单对象
     *
     * @param list                   数据
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, R> void checkThrowAllError(List<T> list, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwAllError(check(list, methodBaseFieldGetters));
    }

    /**
     * 校验表单对象
     *
     * @param list                   数据
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, R> void checkThrowAllError(List<T> list, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwAllError(check(list, requiredType, methodBaseFieldGetters));
    }

    /**
     * 校验表单对象
     *
     * @param map 数据
     * @param <T> 对象类型
     * @param <K> 对象键
     */
    public static <K, T> void checkThrowAllError(Map<K, T> map) {
        throwAllError(check(map));
    }

    /**
     * 校验表单对象
     *
     * @param map          数据
     * @param requiredType 必填验证类型
     * @param <K>          对象键
     * @param <T>          对象类型
     */
    public static <K, T> void checkThrowAllError(Map<K, T> map, FormValidateRequiredType requiredType) {
        throwAllError(check(map, requiredType));
    }

    /**
     * 校验表单对象
     *
     * @param map                    数据
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <K>                    对象键
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, K, R> void checkThrowAllError(Map<K, T> map, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwAllError(check(map, methodBaseFieldGetters));
    }

    /**
     * 校验表单对象
     *
     * @param map                    数据
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <K>                    对象键
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, K, R> void checkThrowAllError(Map<K, T> map, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwAllError(check(map, requiredType, methodBaseFieldGetters));
    }

    /**
     * 检查给定数据，并只抛出第一个检查过程中遇到的错误
     *
     * @param data 数据
     * @param clz  数据类
     * @param <T>  对象类型
     */
    public static <T> void checkThrowFirstError(T data, Class<T> clz) {
        throwFirstError(check(data, clz));
    }

    /**
     * 检查给定数据，并只抛出第一个检查过程中遇到的错误
     *
     * @param list 数据
     * @param clz  数据类
     * @param <T>  对象类型
     */
    public static <T> void checkThrowFirstError(List<T> list, Class<T> clz) {
        throwFirstError(check(list, clz));
    }

    /**
     * 检查给定数据，并只抛出第一个检查过程中遇到的错误
     *
     * @param map 数据
     * @param clz 数据类
     * @param <T> 对象类型
     * @param <K> 对象键
     */
    public static <T, K> void checkThrowFirstError(Map<K, T> map, Class<T> clz) {
        throwFirstError(check(map, clz));
    }

    /**
     * 检查给定数据是否为必填，并只抛出第一个检查过程中遇到的错误
     *
     * @param data         数据
     * @param clz          数据类
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     */
    public static <T> void checkThrowFirstError(T data, Class<T> clz, FormValidateRequiredType requiredType) {
        throwFirstError(check(data, clz, requiredType));
    }

    /**
     * 检查给定数据是否为必填，并只抛出第一个检查过程中遇到的错误
     *
     * @param list         数据
     * @param clz          数据类
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     */
    public static <T> void checkThrowFirstError(List<T> list, Class<T> clz, FormValidateRequiredType requiredType) {
        throwFirstError(check(list, clz, requiredType));
    }

    /**
     * 检查给定数据是否为必填，并只抛出第一个检查过程中遇到的错误
     *
     * @param map          数据
     * @param clz          数据类
     * @param requiredType 必填验证类型
     * @param <K>          对象键
     * @param <T>          对象类型
     */
    public static <T, K> void checkThrowFirstError(Map<K, T> map, Class<T> clz, FormValidateRequiredType requiredType) {
        throwFirstError(check(map, clz, requiredType));
    }

    /**
     * 抛出第一个错误
     *
     * @param data                   数据
     * @param clz                    数据类
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, R> void checkThrowFirstError(T data, Class<T> clz, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwFirstError(check(data, clz, methodBaseFieldGetters));
    }

    /**
     * 抛出第一个错误
     *
     * @param list                   数据
     * @param clz                    数据类
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, R> void checkThrowFirstError(List<T> list, Class<T> clz, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwFirstError(check(list, clz, methodBaseFieldGetters));
    }

    /**
     * 抛出第一个错误
     *
     * @param map                    数据
     * @param clz                    数据类
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @param <K>                    对象键
     */
    @SafeVarargs
    public static <T, K, R> void checkThrowFirstError(Map<K, T> map, Class<T> clz, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwFirstError(check(map, clz, methodBaseFieldGetters));
    }

    /**
     * 抛出第一个错误
     *
     * @param data                   数据
     * @param clz                    数据类
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, R> void checkThrowFirstError(T data, Class<T> clz, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwFirstError(check(data, clz, requiredType, methodBaseFieldGetters));
    }

    /**
     * 抛出第一个错误
     *
     * @param list                   数据
     * @param clz                    数据类
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, R> void checkThrowFirstError(List<T> list, Class<T> clz, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwFirstError(check(list, clz, requiredType, methodBaseFieldGetters));
    }

    /**
     * 抛出第一个错误
     *
     * @param map                    数据
     * @param clz                    数据类
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @param <K>                    对象键
     */
    @SafeVarargs
    public static <T, K, R> void checkThrowFirstError(Map<K, T> map, Class<T> clz, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwFirstError(check(map, clz, requiredType, methodBaseFieldGetters));
    }

    /**
     * 检查给定数据，并抛出所有检查过程中遇到的错误
     *
     * @param data 数据
     * @param clz  数据类
     * @param <T>  对象类型
     */
    public static <T> void checkThrowAllError(T data, Class<T> clz) {
        throwAllError(check(data, clz));
    }

    /**
     * 检查给定数据，并抛出所有检查过程中遇到的错误
     *
     * @param list 数据
     * @param clz  数据类
     * @param <T>  对象类型
     */
    public static <T> void checkThrowAllError(List<T> list, Class<T> clz) {
        throwAllError(check(list, clz));
    }

    /**
     * 检查给定数据，并抛出所有检查过程中遇到的错误
     *
     * @param map 数据
     * @param clz 数据类
     * @param <T> 对象类型
     * @param <K> 对象键
     */
    public static <T, K> void checkThrowAllError(Map<K, T> map, Class<T> clz) {
        throwAllError(check(map, clz));
    }

    /**
     * 检查给定数据是否为必填，并抛出所有检查过程中遇到的错误
     *
     * @param data         数据
     * @param clz          数据类
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     */
    public static <T> void checkThrowAllError(T data, Class<T> clz, FormValidateRequiredType requiredType) {
        throwAllError(check(data, clz, requiredType));
    }

    /**
     * 检查给定数据是否为必填，并抛出所有检查过程中遇到的错误
     *
     * @param list         数据
     * @param clz          数据类
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     */
    public static <T> void checkThrowAllError(List<T> list, Class<T> clz, FormValidateRequiredType requiredType) {
        throwAllError(check(list, clz, requiredType));
    }

    /**
     * 检查给定数据是否为必填，并抛出所有检查过程中遇到的错误
     *
     * @param map          数据
     * @param clz          数据类
     * @param requiredType 必填验证类型
     * @param <T>          对象类型
     * @param <K>          对象键
     */
    public static <T, K> void checkThrowAllError(Map<K, T> map, Class<T> clz, FormValidateRequiredType requiredType) {
        throwAllError(check(map, clz, requiredType));
    }

    /**
     * 批量抛出所有错误
     *
     * @param data                   数据
     * @param clz                    数据类
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, R> void checkThrowAllError(T data, Class<T> clz, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwAllError(check(data, clz, methodBaseFieldGetters));
    }

    /**
     * 批量抛出所有错误
     *
     * @param list                   数据
     * @param clz                    数据类
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, R> void checkThrowAllError(List<T> list, Class<T> clz, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwAllError(check(list, clz, methodBaseFieldGetters));
    }

    /**
     * 批量抛出所有错误
     *
     * @param map                    数据
     * @param clz                    数据类
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @param <K>                    对象键
     */
    @SafeVarargs
    public static <T, K, R> void checkThrowAllError(Map<K, T> map, Class<T> clz, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwAllError(check(map, clz, methodBaseFieldGetters));
    }

    /**
     * 批量抛出所有错误
     *
     * @param data                   数据
     * @param clz                    数据类
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, R> void checkThrowAllError(T data, Class<T> clz, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwAllError(check(data, clz, requiredType, methodBaseFieldGetters));
    }

    /**
     * 批量抛出所有错误
     *
     * @param list                   数据
     * @param clz                    数据类
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     */
    @SafeVarargs
    public static <T, R> void checkThrowAllError(List<T> list, Class<T> clz, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwAllError(check(list, clz, requiredType, methodBaseFieldGetters));
    }

    /**
     * 批量抛出所有错误
     *
     * @param map                    数据
     * @param clz                    数据类
     * @param requiredType           必填验证类型
     * @param methodBaseFieldGetters 方法属性获取器
     * @param <T>                    对象类型
     * @param <R>                    对象值类型
     * @param <K>                    对象键
     */
    @SafeVarargs
    public static <T, K, R> void checkThrowAllError(Map<K, T> map, Class<T> clz, FormValidateRequiredType requiredType, MethodBaseFieldGetter<T, R>... methodBaseFieldGetters) {
        throwAllError(check(map, clz, requiredType, methodBaseFieldGetters));
    }

    /**
     * 或验证
     *
     * @param formValidateRuleData 验证规则
     * @param formValidateItems    验证结果
     * @return 结果
     */
    @SafeVarargs
    public static FormValidateObject<Object> or(FormValidateRuleData formValidateRuleData, FormValidateItem<Object>... formValidateItems) {
        FormValidateObject formValidateObject = new FormValidateObject(null);
        for (FormValidateItem<Object> formValidateItem : formValidateItems) {
            if (formValidateItem.isAllSuccess()) {
                formValidateObject.setResults(ListUtil.of(new FormValidateSuccess()));
                return formValidateObject;
            }
        }
        formValidateObject.setResults(ListUtil.of(new FormValidateError(formValidateRuleData.getMessage())));
        return formValidateObject;
    }

    /**
     * 与验证
     *
     * @param formValidateRuleData 验证规则
     * @param formValidateItems    验证结果
     * @return 结果
     */
    @SafeVarargs
    public static FormValidateObject<Object> and(FormValidateRuleData formValidateRuleData, FormValidateItem<Object>... formValidateItems) {
        FormValidateObject formValidateObject = new FormValidateObject(null);
        for (FormValidateItem<Object> formValidateItem : formValidateItems) {
            if (formValidateItem.isAllError()) {
                formValidateObject.setResults(ListUtil.of(new FormValidateError(formValidateRuleData.getMessage())));
                return formValidateObject;
            }
        }
        formValidateObject.setResults(ListUtil.of(new FormValidateSuccess()));
        return formValidateObject;
    }

    /**
     * 抛出第一个错误
     *
     * @param formValidateItem 验证结果
     */
    public static void throwFirstError(FormValidateItem formValidateItem) {
        throwFirstError(ListUtil.of(formValidateItem));
    }

    /**
     * 抛出第一个错误
     *
     * @param formValidateItems 验证结果
     */
    public static void throwFirstError(List<FormValidateItem> formValidateItems) {
        if (!formValidateItems.isEmpty()) {
            List<FormValidateItem> list = TreeUtil.toList(formValidateItems, FormValidateItem::getItems);
            for (FormValidateItem item : list) {
                List<FormValidateResult> results = item.getResults();
                for (FormValidateResult result : results) {
                    if (result instanceof FormValidateError) {
                        throw new FormValidateResultException(result.getCode(), result.getMessage());
                    }
                }
            }
        }
    }

    /**
     * 批量抛出所有错误
     *
     * @param formValidateItem 验证结果
     */
    public static void throwAllError(FormValidateItem formValidateItem) {
        throwAllError(ListUtil.of(formValidateItem));
    }

    /**
     * 批量抛出所有错误
     *
     * @param formValidateItems 验证结果
     */
    public static void throwAllError(List<FormValidateItem> formValidateItems) {
        if (!formValidateItems.isEmpty()) {
            StringBuilder errors = new StringBuilder();
            List<FormValidateItem> list = TreeUtil.toList(formValidateItems, FormValidateItem::getItems);
            for (FormValidateItem item : list) {
                List<FormValidateResult> results = item.getResults();
                for (FormValidateResult result : results) {
                    if (result instanceof FormValidateError) {
                        if (errors.length() == 0) {
                            errors.append(result.getMessage());
                        } else {
                            errors.append(", ").append(result.getMessage());
                        }
                    }
                }
            }
            if (errors.length() > 0) {
                throw new FormValidateResultException(400, errors.toString());
            }
        }
    }

}