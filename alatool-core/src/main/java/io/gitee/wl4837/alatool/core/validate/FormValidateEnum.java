package io.gitee.wl4837.alatool.core.validate;

import io.gitee.wl4837.alatool.core.validate.validator.DefaultValidator;

/**
 * @author wangliang
 */

public enum FormValidateEnum {

    DEFAULT("default", "默认", "请输入正确的", DefaultValidator.class);

    public final String type;
    public final String name;
    public final String message;
    public final Class<?> handle;

    FormValidateEnum(String type, String name, String message, Class<?> handle) {
        this.type = type;
        this.name = name;
        this.message = message;
        this.handle = handle;
    }

}
