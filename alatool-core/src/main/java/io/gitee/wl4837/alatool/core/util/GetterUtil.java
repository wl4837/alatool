package io.gitee.wl4837.alatool.core.util;

import io.gitee.wl4837.alatool.core.getter.MethodBaseFieldGetter;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * 获取封包数据操作类
 *
 * @author kingood
 */
public class GetterUtil {

    /**
     * 获取字段值
     *
     * @param object 赋值对象
     * @param methodFieldGetter 赋值的方法
     * @return 值
     * @param <P> 对象类型
     * @param <R> 值类型
     */
    @SuppressWarnings("unchecked")
    public static <P, R> R getValue(P object, MethodBaseFieldGetter<P, R> methodFieldGetter) {
        try {
            Method method = methodFieldGetter.getMethod();
            return (R) method.invoke(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取实例属性名称
     *
     * @param methodFieldGetter 对象字段
     * @return 字段名称
     * @param <P> 对象类型
     * @param <R> 值类型
     */
    public static <P, R> String getFieldName(MethodBaseFieldGetter<P, R> methodFieldGetter) {
        try {
            return methodFieldGetter.getFieldName();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取实例属性名称
     *
     * @param methodFieldGetters 对象字段
     * @return 字段名称
     * @param <P> 对象类型
     * @param <R> 值类型
     */
    @SafeVarargs
    public static <P, R> List<String> getFieldName(MethodBaseFieldGetter<P, R>... methodFieldGetters) {
        try {
            List<String> fieldNames = new ArrayList<>();
            for (MethodBaseFieldGetter<P, R> methodFieldGetter : methodFieldGetters) {
                fieldNames.add(methodFieldGetter.getFieldName());
            }
            return fieldNames;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
