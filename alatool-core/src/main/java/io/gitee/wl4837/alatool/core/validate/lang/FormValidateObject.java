package io.gitee.wl4837.alatool.core.validate.lang;

import io.gitee.wl4837.alatool.core.util.TreeUtil;
import io.gitee.wl4837.alatool.core.validate.result.FormValidateError;
import io.gitee.wl4837.alatool.core.validate.FormValidateItem;
import io.gitee.wl4837.alatool.core.validate.FormValidateResult;
import io.gitee.wl4837.alatool.core.validate.result.FormValidateSuccess;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wangliang
 */
public class FormValidateObject<T> implements FormValidateItem<T> {

    protected T value;

    protected List<FormValidateResult> results = new ArrayList<>();

    protected List<FormValidateItem> items = new ArrayList<>();

    public FormValidateObject(T value) {
        this.value = value;
    }

    @Override
    public T getValue() {
        return value;
    }

    @Override
    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public List<FormValidateResult> getResults() {
        return results;
    }

    @Override
    public void setResults(List<FormValidateResult> results) {
        this.results = results;
    }

    @Override
    public List<FormValidateItem> getItems() {
        return items;
    }

    @Override
    public void setItems(List<FormValidateItem> items) {
        this.items = items;
    }

    @Override
    public List<FormValidateResult> getAllResults() {
        List<FormValidateResult> results = new ArrayList<>(this.results);
        List<FormValidateItem> items = TreeUtil.toList(this.items, FormValidateItem::getItems);
        for (FormValidateItem item : items) {
            results.addAll(item.getAllResults());
        }
        return results;
    }

    @Override
    public List<FormValidateResult> getAllErrorResults() {
        List<FormValidateResult> results = getAllResults();
        List<FormValidateResult> errorResults = new ArrayList<>();
        for (FormValidateResult result : results) {
            if (result instanceof FormValidateError) {
                errorResults.add(result);
            }
        }
        return errorResults;
    }

    @Override
    public List<FormValidateResult> getAllSuccessResults() {
        List<FormValidateResult> results = getAllResults();
        List<FormValidateResult> successResults = new ArrayList<>();
        for (FormValidateResult result : results) {
            if (result instanceof FormValidateSuccess) {
                successResults.add(result);
            }
        }
        return successResults;
    }

    @Override
    public List<FormValidateResult> getSuccessResults() {
        List<FormValidateResult> successResults = new ArrayList<>();
        for (FormValidateResult result : this.results) {
            if (result instanceof FormValidateSuccess) {
                successResults.add(result);
            }
        }
        return successResults;
    }

    @Override
    public List<FormValidateResult> getErrorResults() {
        List<FormValidateResult> errorResults = new ArrayList<>();
        for (FormValidateResult result : this.results) {
            if (result instanceof FormValidateError) {
                errorResults.add(result);
            }
        }
        return errorResults;
    }

    @Override
    public Boolean isAllSuccess() {
        return getAllErrorResults().isEmpty();
    }

    @Override
    public Boolean isAllError() {
        return !getAllErrorResults().isEmpty();
    }

    @Override
    public Boolean isSuccess() {
        return getErrorResults().isEmpty();
    }

    @Override
    public Boolean isError() {
        return !getErrorResults().isEmpty();
    }

}
