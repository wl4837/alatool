package io.gitee.wl4837.alatool.core.validate.validator;

import io.gitee.wl4837.alatool.core.util.IdcardUtil;
import io.gitee.wl4837.alatool.core.validate.*;
import io.gitee.wl4837.alatool.core.validate.result.FormValidateError;
import io.gitee.wl4837.alatool.core.validate.result.FormValidateSuccess;

public class IdCardValidator implements FormValidator {


    @Override
    public FormValidateResult handle(Object data, FormValidateRuleData formValidateRuleData) {
        if (null == data) {
            if (formValidateRuleData.getRequired()) {
                return new FormValidateError(formValidateRuleData.getMessage());
            } else {
                return new FormValidateSuccess();
            }
        }
        String idcard = data.toString();
        if ("".equals(idcard)) {
            if (formValidateRuleData.getRequired()) {
                return new FormValidateError(formValidateRuleData.getMessage());
            } else {
                return new FormValidateSuccess();
            }
        }
        if (IdcardUtil.isValidCard(idcard)) {
            return new FormValidateSuccess();
        } else {
            return new FormValidateError(formValidateRuleData.getMessage());
        }
    }

}
