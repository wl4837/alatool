package io.gitee.wl4837.alatool.core.validate;

/**
 * @author wangliang
 */
public interface FormValidator {

    FormValidateResult handle(Object data, FormValidateRuleData formValidateRuleData);

}
