package io.gitee.wl4837.alatool.core.lang;

import java.util.Date;

public class JsDate implements JsVar, java.io.Serializable, Comparable<JsDate> {

    @SuppressWarnings("all")
    public static final Class<Boolean> TYPE = Boolean.class;

    private Date value;

    public JsDate() {
        this.value = new Date();
    }

    public JsDate(Date original) {
        this.value = original;
    }

    public Date getValue() {
        return this.value;
    }

    public void setValue(Date value) {
        this.value = value;
    }

    @Override
    public int compareTo(JsDate o) {
        return o.compareTo(this);
    }

}
