package io.gitee.wl4837.alatool.core.validate;

import java.sql.JDBCType;

/**
 * @author wangliang
 */
public enum FormValidateRequiredType {

    NONE(0, "规则验证"),
    REQUIRED(1, "强制验证"),
    OPTIONAL(3, "强制不验证");

    private Integer type;

    private String description;

    FormValidateRequiredType(Integer type) {
        this.type = type;
    }

    FormValidateRequiredType(Integer type, String description) {
        this.type = type;
        this.description = description;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name();
    }

}
