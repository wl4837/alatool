package io.gitee.wl4837.alatool.core.lang;

public class JsSymbol implements JsVar, java.io.Serializable, Comparable<JsSymbol> {

    private final String value;

    @SuppressWarnings("all")
    public static final Class<String> TYPE = String.class;

    public JsSymbol() {
        this.value = "";
    }

    public JsSymbol(String original) {
        this.value = original;
    }

    public int indexOf(String str) {
        return value.indexOf(str);
    }

    @Override
    public int compareTo(JsSymbol o) {
        return 0;
    }

}
