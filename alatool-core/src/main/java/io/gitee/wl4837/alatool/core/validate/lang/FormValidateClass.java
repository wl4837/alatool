package io.gitee.wl4837.alatool.core.validate.lang;

/**
 * @author wangliang
 */
public class FormValidateClass<T> extends FormValidateObject<T> {

    private Class<T> clz;

    public FormValidateClass(T value, Class<T> clz) {
        super(value);
        this.clz = clz;
    }

    public Class<T> getClz() {
        return clz;
    }

    public void setClz(Class<T> clz) {
        this.clz = clz;
    }

}
