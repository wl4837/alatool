package io.gitee.wl4837.alatool.core.lang;

/**
 * @author kingood
 */
public class MethodType {

    private Class<?> returnType;
    private Class<?>[] parameterTypes;

    public Class<?> getReturnType() {
        return returnType;
    }

    public void setReturnType(Class<?> returnType) {
        this.returnType = returnType;
    }

    public Class<?>[] getParameterTypes() {
        return parameterTypes;
    }

    public void setParameterTypes(Class<?>[] parameterTypes) {
        this.parameterTypes = parameterTypes;
    }

}
