package io.gitee.wl4837.alatool.core.lang;

public class JsUndefined implements JsVar, java.io.Serializable {

    public static final JsUndefined UNDEFINED = new JsUndefined();

    private String value;

    @Override
    public String toString() {
        return "undefined";
    }

    @Override
    public boolean equals(Object obj) {
        if (null == obj) {
            return true;
        }
        return JsUndefined.class.equals(obj.getClass());
    }

}
