package io.gitee.wl4837.alatool.core.validate.result;

import io.gitee.wl4837.alatool.core.validate.FormValidateResult;

/**
 * @author wangliang
 */
public class FormValidateIgnore implements FormValidateResult {

    private int code = 0;

    private String message;

    public FormValidateIgnore() {

    }

    public FormValidateIgnore(String message) {
        this.message = message;
    }

    public FormValidateIgnore(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

}
