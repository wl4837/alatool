package io.gitee.wl4837.alatool.core.lang;

/**
 * 通用请求响应枚举类
 *
 * @author wangliang
 */
public enum HttpStatusCode {

    CONTINUE(100, "Continue", "继续。客户端应继续其请求"),
    SWITCHING_PROTOCOLS(101, "Switching Protocols", "切换协议。服务器根据客户端的请求切换协议。只能切换到更高级的协议，例如，切换到HTTP的新版本协议"),
    OK(200, "OK", "请求成功"),
    CREATED(201, "Created", "已创建。成功请求并创建了新的资源"),
    ACCEPTED(202, "Accepted", "已接受。已经接受请求，但未处理完成"),
    NON_AUTHORITATIVE_INFORMATION(203, "Non-Authoritative Information", "非授权信息。请求成功。但返回的meta信息不在原始的服务器，而是一个副本"),
    NO_CONTENT(204, "No Content", "已接受。已经接受请求，但未处理完成"),
    MULTIPLE_CHOICES(300, "Multiple Choices", "多种选择。请求的资源可包括多个位置，相应可返回一个资源特征与地址的列表用于用户终端（例如：浏览器）选择"),
    MOVED_PERMANENTLY(301, "Moved Permanently", "永久移动。请求的资源已被永久的移动到新URI，返回信息会包括新的URI，浏览器会自动定向到新URI。今后任何新的请求都应使用新的URI代替"),
    FOUND(302, "Found", "临时移动。与301类似。但资源只是临时被移动。客户端应继续使用原有URI"),
    SEE_OTHER(303, "See Other", "查看其它地址。与301类似。使用GET和POST请求查看"),
    NOT_MODIFIED(304, "Not Modified", "未修改。所请求的资源未修改，服务器返回此状态码时，不会返回任何资源。客户端通常会缓存访问过的资源，通过提供一个头信息指出客户端希望只返回在指定日期之后修改的资源"),
    USE_PROXY(305, "Use Proxy", "使用代理。所请求的资源必须通过代理访问"),
    BAD_REQUEST(400, "Bad Request", "客户端请求的语法错误，服务器无法理解"),
    UNAUTHORIZED(401, "Unauthorized", "请求要求用户的身份认证"),
    PAYMENT_REQUIRED(402, "Payment Required", "保留，将来使用"),
    FORBIDDEN(403, "Forbidden", "客户端请求的语法错误，服务器无法理解"),
    NOT_FOUND(404, "Not Found", "服务器无法根据客户端的请求找到资源"),
    METHOD_NOT_ALLOWED(405, "Method Not Allowed", "客户端请求中的方法被禁止"),
    NOT_ACCEPTABLE(406, "Not Acceptable", "服务器无法根据客户端请求的内容特性完成请求"),
    PROXY_AUTHENTICATION_REQUIRED(407, "Proxy Authentication Required", "请求要求代理的身份认证，与401类似，但请求者应当使用代理进行授权"),
    REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested range not satisfiable", "客户端请求的范围无效"),
    INTERNAL_SERVER_ERROR(500, "Internal Server Error", "服务器内部错误，无法完成请求"),
    NOT_IMPLEMENTED(501, "Not Implemented", "服务器不支持请求的功能，无法完成请求"),
    BAD_GATEWAY(502, "Bad Gateway", "作为网关或者代理工作的服务器尝试执行请求时，从远程服务器接收到了一个无效的响应"),
    SERVICE_UNAVAILABLE(502, "Service Unavailable", "由于超载或系统维护，服务器暂时的无法处理客户端的请求。延时的长度可包含在服务器的Retry-After头信息中"),
    GATEWAY_TIME_OUT(504, "Gateway Time-out", "充当网关或代理的服务器，未及时从远端服务器获取请求"),
    HTTP_VERSION_NOT_SUPPORTED(505, "HTTP Version not supported", "服务器不支持请求的HTTP协议的版本，无法完成处理");

    HttpStatusCode(Integer code) {
        this.code = code;
    }

    HttpStatusCode(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    HttpStatusCode(Integer code, String name, String description) {
        this.code = code;
        this.name = name;
        this.description = description;
    }

    private Integer code;

    private String name;

    private String description;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
