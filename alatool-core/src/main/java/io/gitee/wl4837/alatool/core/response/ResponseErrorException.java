package io.gitee.wl4837.alatool.core.response;

/**
 * @author wangliang
 */
public class ResponseErrorException extends ResponseException {


    public ResponseErrorException(String message) {
        super(message);
        this.code = 500;
    }

    public ResponseErrorException(String message, Throwable e) {
        super(message, e);
        this.code = 500;
    }

    public ResponseErrorException(Integer code, String message) {
        super(code, message);
    }

    public ResponseErrorException(Integer code, String message, Throwable e) {
        super(code, message, e);
    }

}
