package io.gitee.wl4837.alatool.core.validate;

import io.gitee.wl4837.alatool.core.validate.validator.DefaultValidator;

import java.util.List;

/**
 * @author wangliang
 */
public class FormValidateRuleData {

    private Boolean required = false;

    private String message = "请输入值";

    private Integer min = 0;

    private Integer mix = 0;

    private Class<? extends FormValidator> validator = DefaultValidator.class;

    private String[] resultStrings = {};

    private int[] resultIntegers = {};

    private long[] resultLongs = {};

    private boolean[] resultBooleans = {};

    private double[] resultDoubles = {};

    private float[] resultFloats = {};

    public FormValidateRuleData() {

    }

    public FormValidateRuleData(String message) {
        this.message = message;
    }

    public FormValidateRuleData(String message, Boolean required) {
        this.message = message;
        this.required = required;
    }

    public FormValidateRuleData(Boolean required, String message, Integer min, Integer mix, Class<? extends FormValidator> validator, String[] resultStrings, int[] resultIntegers, long[] resultLongs, boolean[] resultBooleans, double[] resultDoubles, float[] resultFloats) {
        this.required = required;
        this.message = message;
        this.min = min;
        this.mix = mix;
        this.validator = validator;
        this.resultStrings = resultStrings;
        this.resultIntegers = resultIntegers;
        this.resultLongs = resultLongs;
        this.resultBooleans = resultBooleans;
        this.resultDoubles = resultDoubles;
        this.resultFloats = resultFloats;
    }

    public String[] getResultStrings() {
        return resultStrings;
    }

    public void setResultStrings(String[] resultStrings) {
        this.resultStrings = resultStrings;
    }

    public int[] getResultIntegers() {
        return resultIntegers;
    }

    public void setResultIntegers(int[] resultIntegers) {
        this.resultIntegers = resultIntegers;
    }

    public long[] getResultLongs() {
        return resultLongs;
    }

    public void setResultLongs(long[] resultLongs) {
        this.resultLongs = resultLongs;
    }

    public boolean[] getResultBooleans() {
        return resultBooleans;
    }

    public void setResultBooleans(boolean[] resultBooleans) {
        this.resultBooleans = resultBooleans;
    }

    public double[] getResultDoubles() {
        return resultDoubles;
    }

    public void setResultDoubles(double[] resultDoubles) {
        this.resultDoubles = resultDoubles;
    }

    public float[] getResultFloats() {
        return resultFloats;
    }

    public void setResultFloats(float[] resultFloats) {
        this.resultFloats = resultFloats;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Integer getMix() {
        return mix;
    }

    public void setMix(Integer mix) {
        this.mix = mix;
    }

    public Class<? extends FormValidator> getValidator() {
        return validator;
    }

    public void setValidator(Class<? extends FormValidator> validator) {
        this.validator = validator;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
