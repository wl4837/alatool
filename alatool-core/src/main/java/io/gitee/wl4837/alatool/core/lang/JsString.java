package io.gitee.wl4837.alatool.core.lang;


public final class JsString implements JsVar, java.io.Serializable, Comparable<JsString> {

    private static final long serialVersionUID = -6849794470754667710L;

    private String value;

    @SuppressWarnings("all")
    public static final Class<String> TYPE = String.class;

    public JsString() {
        this.value = "";
    }

    public JsString(String original) {
        this.value = original;
    }

    public int indexOf(String str) {
        return value.indexOf(str);
    }

    @Override
    public int compareTo(JsString o) {
        return value.compareTo(o.value);
    }

    public String toString() {
        return this.value;
    }

    public char[] toCharArray() {
        return value.toCharArray();
    }

    @Override
    @SuppressWarnings("all")
    public boolean equals(Object obj) {
        return value.equals(obj);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static JsString valueOf(JsString value) {
        return new JsString(value.getValue());
    }

    public static JsString valueOf(String value) {
        return new JsString(value);
    }

}
