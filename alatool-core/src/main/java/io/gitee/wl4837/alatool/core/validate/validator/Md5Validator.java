package io.gitee.wl4837.alatool.core.validate.validator;

import io.gitee.wl4837.alatool.core.validate.*;
import io.gitee.wl4837.alatool.core.validate.result.FormValidateError;
import io.gitee.wl4837.alatool.core.validate.result.FormValidateSuccess;

public class Md5Validator implements FormValidator {

    @Override
    public FormValidateResult handle(Object data, FormValidateRuleData formValidateRuleData) {
        if (null == data) {
            if (formValidateRuleData.getRequired()) {
                return new FormValidateError(formValidateRuleData.getMessage());
            } else {
                return new FormValidateSuccess();
            }
        }
        String md5 = data.toString();
        if ("".equals(md5)) {
            if (formValidateRuleData.getRequired()) {
                return new FormValidateError(formValidateRuleData.getMessage());
            } else {
                return new FormValidateSuccess();
            }
        }
        if (md5.replaceAll("-", "").length() == 32) {
            return new FormValidateSuccess();
        } else {
            return new FormValidateError(formValidateRuleData.getMessage());
        }
    }

}
