package io.gitee.wl4837.alatool.core.lang;

public class JsRegExp implements JsVar, java.io.Serializable, Comparable<JsRegExp> {

    private final String value;

    @SuppressWarnings("all")
    public static final Class<String> TYPE = String.class;

    public JsRegExp() {
        this.value = "";
    }

    public JsRegExp(String original) {
        this.value = original;
    }

    public int indexOf(String str) {
        return value.indexOf(str);
    }

    @Override
    public int compareTo(JsRegExp o) {
        return 0;
    }

}
