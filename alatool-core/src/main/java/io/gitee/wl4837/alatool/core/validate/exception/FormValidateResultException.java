package io.gitee.wl4837.alatool.core.validate.exception;

/**
 * @author wangliang
 */
public class FormValidateResultException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * 状态
     */
    private Integer code;

    /**
     * 描述状态
     */
    private String name;

    /**
     * 状态描述
     */
    private String message;

    /**
     * 返回数据
     */
    private Object[] data;

    /**
     * 时间戳
     */
    private Long timestamp;

    public FormValidateResultException() {
        super();
    }

    public FormValidateResultException(String message) {
        super(message);
        this.message = message;
    }

    public FormValidateResultException(Integer code) {
        this.code = code;
    }

    public FormValidateResultException(Integer code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public FormValidateResultException(String message, Throwable cause) {
        super(message, cause);
        this.message = message;
    }

    public FormValidateResultException(Integer code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public FormValidateResultException(Integer code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.message = message;
    }

    public FormValidateResultException(Throwable cause) {
        super(cause);
    }

    protected FormValidateResultException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object[] getData() {
        return data;
    }

    public void setData(Object[] data) {
        this.data = data;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

}
