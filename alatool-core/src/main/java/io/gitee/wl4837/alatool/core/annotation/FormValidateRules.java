package io.gitee.wl4837.alatool.core.annotation;

import java.lang.annotation.*;

/**
 * 表单验证规则
 *
 * @author wangliang
 */
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FormValidateRules {

    /**
     * @return 表单验证规则集合
     */
    FormValidateRule[] value();

}