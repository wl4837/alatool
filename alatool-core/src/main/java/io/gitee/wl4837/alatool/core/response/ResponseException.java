package io.gitee.wl4837.alatool.core.response;

/**
 * @author wangliang
 */
public class ResponseException extends RuntimeException {

    /**
     * 描述状态
     */
    protected Integer code;

    /**
     * 状态描述
     */
    protected String message;

    /**
     * 返回数据
     */
    protected Object data;


    public ResponseException(String message) {
        super(message);
        this.code = 500;
        this.message = message;
    }

    public ResponseException(String message, Throwable e) {
        super(message, e);
        this.code = 500;
        this.message = message;
    }

    public ResponseException(Integer code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public ResponseException(Integer code, String message, Throwable e) {
        super(message, e);
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
