package io.gitee.wl4837.alatool.core.validate.validator;

import io.gitee.wl4837.alatool.core.lang.JsNull;
import io.gitee.wl4837.alatool.core.lang.JsUndefined;
import io.gitee.wl4837.alatool.core.validate.*;
import io.gitee.wl4837.alatool.core.validate.result.FormValidateError;
import io.gitee.wl4837.alatool.core.validate.result.FormValidateIgnore;
import io.gitee.wl4837.alatool.core.validate.result.FormValidateSuccess;

import java.util.List;
import java.util.Map;

public class DefaultValidator implements FormValidator {

    @Override
    public FormValidateResult handle(Object data, FormValidateRuleData formValidateRuleData) {
        if (null == data) {
            if (formValidateRuleData.getRequired()) {
                return new FormValidateError(formValidateRuleData.getMessage());
            } else {
                return new FormValidateSuccess();
            }
        } else if (data instanceof JsUndefined) {
            return new FormValidateIgnore();
        } else if (data instanceof JsNull) {
            if (formValidateRuleData.getRequired()) {
                return new FormValidateError(formValidateRuleData.getMessage());
            } else {
                return new FormValidateSuccess();
            }
        } else if (data instanceof String) {
            if ("" == data) {
                if (formValidateRuleData.getRequired()) {
                    return new FormValidateError(formValidateRuleData.getMessage());
                } else {
                    return new FormValidateSuccess();
                }
            }
            if (formValidateRuleData.getResultStrings().length > 0) {
                boolean legalValue = true;
                for (String result : formValidateRuleData.getResultStrings()) {
                    if (result.equals(data)) {
                        legalValue = false;
                        break;
                    }
                }
                if (legalValue) {
                    return new FormValidateError(formValidateRuleData.getMessage());
                }
            }
            if (0 != formValidateRuleData.getMin()) {
                if (((String) data).length() < formValidateRuleData.getMin()) {
                    return new FormValidateError(formValidateRuleData.getMessage());
                }
            }
            if (0 != formValidateRuleData.getMix()) {
                if (((String) data).length() >= formValidateRuleData.getMix()) {
                    return new FormValidateError(formValidateRuleData.getMessage());
                }
            }
            return new FormValidateSuccess();
        } else if (data instanceof Integer) {
            if (0 == (Integer) data) {
                if (formValidateRuleData.getRequired()) {
                    return new FormValidateError(formValidateRuleData.getMessage());
                } else {
                    return new FormValidateSuccess();
                }
            }
            if (formValidateRuleData.getResultIntegers().length > 0) {
                boolean legalValue = true;
                for (Integer result : formValidateRuleData.getResultIntegers()) {
                    if (result.equals(data)) {
                        legalValue = false;
                        break;
                    }
                }
                if (legalValue) {
                    return new FormValidateError(formValidateRuleData.getMessage());
                }
            }
            if (0 != formValidateRuleData.getMin()) {
                if (((Integer) data) < formValidateRuleData.getMin()) {
                    return new FormValidateError(formValidateRuleData.getMessage());
                }
            }
            if (0 != formValidateRuleData.getMix()) {
                if (((Integer) data) >= formValidateRuleData.getMix()) {
                    return new FormValidateError(formValidateRuleData.getMessage());
                }
            }
            return new FormValidateSuccess();
        } else if (data instanceof List) {
            if (((List<?>) data).isEmpty()) {
                if (formValidateRuleData.getRequired()) {
                    return new FormValidateError(formValidateRuleData.getMessage());
                } else {
                    return new FormValidateSuccess();
                }
            }
            for (Object object : ((List<?>) data)) {
                if (object instanceof String) {
                    if (formValidateRuleData.getResultStrings().length > 0) {
                        boolean legalValue = true;
                        for (Object result : formValidateRuleData.getResultStrings()) {
                            if (result.equals(object)) {
                                legalValue = false;
                                break;
                            }
                        }
                        if (legalValue) {
                            return new FormValidateError(formValidateRuleData.getMessage());
                        }
                    }
                } else if (object instanceof Integer) {
                    boolean legalValue = true;
                    for (Object result : formValidateRuleData.getResultIntegers()) {
                        if (result.equals(object)) {
                            legalValue = false;
                            break;
                        }
                    }
                    if (legalValue) {
                        return new FormValidateError(formValidateRuleData.getMessage());
                    }
                }
            }
            if (0 != formValidateRuleData.getMin()) {
                if (((List) data).size() < formValidateRuleData.getMin()) {
                    return new FormValidateError(formValidateRuleData.getMessage());
                }
            }
            if (0 != formValidateRuleData.getMix()) {
                if (((List) data).size() >= formValidateRuleData.getMix()) {
                    return new FormValidateError(formValidateRuleData.getMessage());
                }
            }
        } else if (data instanceof Map) {
            if (((Map) data).isEmpty()) {
                if (formValidateRuleData.getRequired()) {
                    return new FormValidateError(formValidateRuleData.getMessage());
                } else {
                    return new FormValidateSuccess();
                }
            }
            for (Object object : ((Map) data).values()) {
                if (object instanceof String) {
                    if (formValidateRuleData.getResultStrings().length > 0) {
                        boolean legalValue = true;
                        for (Object result : formValidateRuleData.getResultStrings()) {
                            if (result.equals(object)) {
                                legalValue = false;
                                break;
                            }
                        }
                        if (legalValue) {
                            return new FormValidateError(formValidateRuleData.getMessage());
                        }
                    }
                } else if (object instanceof Integer) {
                    boolean legalValue = true;
                    for (Object result : formValidateRuleData.getResultIntegers()) {
                        if (result.equals(object)) {
                            legalValue = false;
                            break;
                        }
                    }
                    if (legalValue) {
                        return new FormValidateError(formValidateRuleData.getMessage());
                    }
                }
            }
            if (0 != formValidateRuleData.getMin()) {
                if (((Map) data).size() < formValidateRuleData.getMin()) {
                    return new FormValidateError(formValidateRuleData.getMessage());
                }
            }
            if (0 != formValidateRuleData.getMix()) {
                if (((Map) data).size() >= formValidateRuleData.getMix()) {
                    return new FormValidateError(formValidateRuleData.getMessage());
                }
            }
        }
        return new FormValidateSuccess();
    }

}
