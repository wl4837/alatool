package io.gitee.wl4837.alatool.core.validate;

import java.io.Serializable;
import java.util.List;

/**
 * @author wangliang
 */
public interface FormValidateItem<T> extends Serializable {

    T getValue();

    void setValue(T value);

    List<FormValidateResult> getResults();

    void setResults(List<FormValidateResult> results);

    List<FormValidateItem> getItems();

    void setItems(List<FormValidateItem> items);

    List<FormValidateResult> getAllResults();

    List<FormValidateResult> getAllSuccessResults();

    List<FormValidateResult> getAllErrorResults();

    List<FormValidateResult> getSuccessResults();

    List<FormValidateResult> getErrorResults();

    Boolean isAllSuccess();

    Boolean isAllError();

    Boolean isSuccess();

    Boolean isError();

}
