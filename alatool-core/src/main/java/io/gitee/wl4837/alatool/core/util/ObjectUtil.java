package io.gitee.wl4837.alatool.core.util;

import cn.hutool.core.util.ObjUtil;
import io.gitee.wl4837.alatool.core.getter.MethodBaseFieldGetter;

/**
 * 对象工具类，包括判空、克隆、序列化等操作
 *
 * @author Kingood
 */
public class ObjectUtil extends ObjUtil {

    /**
     * 获取实例属性名称
     *
     * @param methodFieldGetter 对象字段
     * @return 实例属性名称
     * @param <P> 类
     * @param <R> 属性
     */
    public static <P, R> String getMethodName(MethodBaseFieldGetter<P, R> methodFieldGetter) {
        try {
            return methodFieldGetter.getMethodName();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
