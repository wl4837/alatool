package io.gitee.wl4837.alatool.core;

import io.gitee.wl4837.alatool.core.annotation.FormValidateRule;
import io.gitee.wl4837.alatool.core.util.FormValidateUtil;
import io.gitee.wl4837.alatool.core.validate.*;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


public class TestDataTest {

    @Test
    @Ignore
    public void test() {
        FormValidateUtil.throwAllError(
                FormValidateUtil.and(
                        new FormValidateRuleData("参数都不能为空"),
                        FormValidateUtil.or(
                                new FormValidateRuleData("参数都不能为空2"),
                                FormValidateUtil.check("66", new FormValidateRuleData("请输入内容", true)),
                                FormValidateUtil.check("66", new FormValidateRuleData("请输入内容", true)),
                                FormValidateUtil.check(null, new FormValidateRuleData("请输入内容", true))
                        ),
                        FormValidateUtil.or(
                                new FormValidateRuleData("参数都不能为空1"),
                                FormValidateUtil.check("66", new FormValidateRuleData("请输入内容", true)),
                                FormValidateUtil.check(null, new FormValidateRuleData("请输入内容", true)),
                                FormValidateUtil.check(null, new FormValidateRuleData("请输入内容", true))
                        )
                )
        );
    }

}
