package io.gitee.wl4837.alatool.core;

import io.gitee.wl4837.alatool.core.annotation.FormValidateRule;
import io.gitee.wl4837.alatool.core.annotation.FormValidateRules;

@FormValidateRule(message = "请输入Member对象")
public class Member {

    public String b;

    @FormValidateRules(value = {
            @FormValidateRule(message = "请输入客户名称", required = false),
            @FormValidateRule(message = "客户名称长度必须大于2", min = 2),
            @FormValidateRule(message = "客户名称长度必须小于10", mix = 10),
    })
    private String memberName;

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

}
