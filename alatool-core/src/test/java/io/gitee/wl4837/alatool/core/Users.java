package io.gitee.wl4837.alatool.core;

import io.gitee.wl4837.alatool.core.annotation.FormValidateRule;
import io.gitee.wl4837.alatool.core.annotation.FormValidateRules;
import io.gitee.wl4837.alatool.core.validate.validator.EmailValidator;
import io.gitee.wl4837.alatool.core.validate.validator.IdCardValidator;
import io.gitee.wl4837.alatool.core.validate.validator.Md5Validator;
import io.gitee.wl4837.alatool.core.validate.validator.PhoneValidator;

import java.util.List;

@FormValidateRule(message = "请输入Users对象", required = true)
public class Users extends Member {

    public String a;

    @FormValidateRule(message = "请输入编号", validator = Md5Validator.class, required = true)
    private String id;

    @FormValidateRules(value = {
            @FormValidateRule(message = "请输入名称", required = true),
            @FormValidateRule(message = "名称长度必须大于2", min = 2),
            @FormValidateRule(message = "名称长度必须小于10", mix = 10),
    })
    private String name;

    @FormValidateRules(value = {
            @FormValidateRule(message = "请输入手机号", required = true),
            @FormValidateRule(message = "请输入正确手机号", validator = PhoneValidator.class),
    })
    private String phone;

    @FormValidateRules(value = {
            @FormValidateRule(message = "请输入身份证"),
            @FormValidateRule(message = "请输入正确身份证", validator = IdCardValidator.class),
    })
    private String idcard;

    @FormValidateRules(value = {
            @FormValidateRule(message = "请输入密码", min = 6, required = true),
            @FormValidateRule(message = "密码长度必须大于6", min = 6),
            @FormValidateRule(message = "密码长度必须小于20", mix = 20),
    })
    private String password;

    @FormValidateRule(message = "请输入年龄", min = 18, mix = 150, resultIntegers = {18, 36, 88}, required = true)
    private Integer age;

    @FormValidateRule(message = "请输入性别", resultStrings = {"男", "女"}, required = true)
    private String sex;

    @FormValidateRule(message = "请输入地址")
    private String address;

    @FormValidateRule(message = "请输入邮箱", validator = EmailValidator.class)
    private String email;

    @FormValidateRule(message = "请输入销售", validator = Md5Validator.class)
    private String userId;

    @FormValidateRules(value = {
            @FormValidateRule(message = "请输入爱好", required = true),
            @FormValidateRule(message = "请输入正确爱好", resultStrings = {"自行车", "游泳", "跑步"}),
            @FormValidateRule(message = "爱好不能少于3个", min = 3),
            @FormValidateRule(message = "爱好不能多于6个", mix = 6),
    })
    private List<String> hobby;

    @FormValidateRule(message = "请输入备注")
    private String remark;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<String> getHobby() {
        return hobby;
    }

    public void setHobby(List<String> hobby) {
        this.hobby = hobby;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

}
