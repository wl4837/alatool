package io.gitee.wl4837.alatool.core.util;

import io.gitee.wl4837.alatool.core.Users;
import io.gitee.wl4837.alatool.core.validate.*;
import io.gitee.wl4837.alatool.core.validate.exception.FormValidateResultException;
import io.gitee.wl4837.alatool.core.validate.lang.FormValidateMap;
import io.gitee.wl4837.alatool.core.validate.lang.FormValidateObject;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 数据字段验证工具测试
 */
public class FormValidateUtilTest {

    /**
     * 简单的验证 返回结果
     */
    @Test
    @Ignore
    public void simpleTest() {
//        验证对象
        Users users = new Users();
//        使用验证器验证
        FormValidateObject formValidateObject = FormValidateUtil.check(users);
        try {
//            使用验证器抛出所有错误
            FormValidateUtil.throwAllError(formValidateObject);
        } catch (FormValidateResultException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 简单的验证 返回结果
     */
    @Test
    @Ignore
    public void simpleListTest() {
        List<Users> list = new ArrayList<>();
//        验证对象
        Users users = new Users();
        list.add(users);
        list.add(users);
//        使用验证器验证
        FormValidateObject formValidateObject = FormValidateUtil.check(list);
        try {
//            使用验证器抛出所有错误
            FormValidateUtil.throwAllError(formValidateObject);
        } catch (FormValidateResultException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 简单的验证 返回结果
     */
    @Test
    @Ignore
    public void simpleMapTest() {
        HashMap<String, Users> list = new HashMap<>();
//        验证对象
        Users users = new Users();
        list.put("1", users);
        list.put("2", users);
//        使用验证器验证
        FormValidateMap<Users> map = FormValidateUtil.check(list);
        try {
//            使用验证器抛出所有错误
            FormValidateUtil.throwAllError(map);
        } catch (FormValidateResultException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 简单的验证 返回结果
     */
    @Test
    @Ignore
    public void simpleListThrowFirstErrorTest() {
        List<Users> list = new ArrayList<>();
//        验证对象
        Users users = new Users();
        list.add(users);
        list.add(null);
        try {
//            使用验证器抛出所有错误
            FormValidateUtil.checkThrowFirstError(list, Users.class);
        } catch (FormValidateResultException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 简单的验证 返回结果
     */
    @Test
    @Ignore
    public void simpleListThrowAllErrorTest() {
        List<Users> list = new ArrayList<>();
//        验证对象
        Users users = new Users();
        list.add(users);
        list.add(users);
        try {
//            使用验证器抛出所有错误
            FormValidateUtil.checkThrowAllError(list);
        } catch (FormValidateResultException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 简单的抛出第一个错误异常
     */
    @Test
    @Ignore
    public void simpleThrowFirstErrorTest() {
//        验证对象
        Users users = new Users();
        try {
//        使用验证器验证并抛出第一个错误
            FormValidateUtil.checkThrowFirstError(users);
        } catch (FormValidateResultException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 简单的抛出所有错误异常
     */
    @Test
    @Ignore
    public void simpleThrowAllErrorTest() {
//        验证对象
        Users users = new Users();
        try {
//        使用验证器验证并抛出所有错误
            FormValidateUtil.checkThrowAllError(users);
        } catch (FormValidateResultException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 简单的验证为空的对象
     */
    @Test
    @Ignore
    public void simpleNullObjectTest() {
//        验证对象
        Users users = new Users();
        try {
//        使用验证器验证并抛出所有错误
            FormValidateUtil.checkThrowAllError(users, Users.class);
        } catch (FormValidateResultException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 简单的验证对象下面的个别属性, 使用Get属性获取器
     */
    @Test
    @Ignore
    public void simpleFieldGetterTest() {
//        验证对象
        Users users = new Users();
        try {
//        使用验证器验证并抛出所有错误
            FormValidateUtil.checkThrowAllError(users, Users::getName, Users::getUserId, Users::getEmail);
        } catch (FormValidateResultException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 简单的验证对象联合条件
     */
    @Test
    @Ignore
    public void simpleOrAndTest() {
//        验证对象
        Users users = new Users();
        try {
//        使用验证器验证并抛出所有错误
            FormValidateUtil.throwAllError(
                    FormValidateUtil.and(
                            new FormValidateRuleData("参数不能为空"),
                            FormValidateUtil.or(
                                    new FormValidateRuleData("姓名或者编号不能为空"),
                                    FormValidateUtil.check(users, Users.class, FormValidateRequiredType.REQUIRED, Users::getName),
                                    FormValidateUtil.check(users, Users.class, FormValidateRequiredType.REQUIRED, Users::getUserId)
                            ),
                            FormValidateUtil.or(
                                    new FormValidateRuleData("姓名或者地址不能为空"),
                                    FormValidateUtil.check(users, Users.class, FormValidateRequiredType.REQUIRED, Users::getName),
                                    FormValidateUtil.check(users, Users.class, FormValidateRequiredType.REQUIRED, Users::getAddress)
                            )
                    )
            );
        } catch (FormValidateResultException e) {
            System.out.println(e.getMessage());
        }
    }

}
