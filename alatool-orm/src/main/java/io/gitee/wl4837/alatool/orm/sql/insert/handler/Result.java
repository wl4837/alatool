package io.gitee.wl4837.alatool.orm.sql.insert.handler;

/**
 * 结果处理
 *
 * @author kingood
 */
public interface Result<T> {

    default long exec(){
        return 0;
    }

}
