package io.gitee.wl4837.alatool.orm.sql.select.handler;

import java.util.ArrayList;
import java.util.List;

/**
 * 结果处理
 *
 * @author kingood
 */
public interface Result<T> {

    default List<T> get() {
        return new ArrayList<>();
    }

    @SuppressWarnings("unchecked")
    default T first() {
        return (T) new Object() { };
    }

    default long count() {
        return 0;
    }

    default String toSql() {
        return "";
    }

}
