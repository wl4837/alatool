package io.gitee.wl4837.alatool.orm.sql.update;

import io.gitee.wl4837.alatool.orm.sql.SqlBuilder;
import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;
import io.gitee.wl4837.alatool.orm.sql.update.handler.Result;
import io.gitee.wl4837.alatool.orm.sql.base.InFunc;

/**
 * @author kingood
 */
public class WhereIn<T> extends SqlBuilderItem implements Result<T> {

    private final SqlBuilder sqlBuilder;

    public WhereIn(SqlBuilder sqlBuilder, InFunc<Where<T>, WhereOn<T>> where) {
        this.sqlBuilder = sqlBuilder;
    }

    public WhereOr<T> or() {
        return new WhereOr<>(this.sqlBuilder);
    }

    public WhereAnd<T> and() {
        return new WhereAnd<>(this.sqlBuilder);
    }

    public Limit<T> limit(Integer count) {
        return new Limit<>(this.sqlBuilder, count);
    }

    public Limit<T> limit(Integer offset, Integer count) {
        return new Limit<>(this.sqlBuilder, offset, count);
    }

}
