package io.gitee.wl4837.alatool.orm.sql.select;

import io.gitee.wl4837.alatool.orm.sql.SqlBuilder;
import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;
import io.gitee.wl4837.alatool.orm.sql.column.handler.Result;
import io.gitee.wl4837.alatool.orm.sql.base.Condition;

/**
 * @author kingood
 */
public class LeftJoinOn<T> extends SqlBuilderItem implements io.gitee.wl4837.alatool.orm.sql.select.handler.Result<T> {

    private final SqlBuilder sqlBuilder;

    public LeftJoinOn(SqlBuilder sqlBuilder, Result result1, Result result2) {
        this.sqlBuilder = sqlBuilder;
    }

    public LeftJoinOn(SqlBuilder sqlBuilder, Result result1, Condition condition, Result result2) {
        this.sqlBuilder = sqlBuilder;
    }

    public Where<T> where() {
        return new Where<>(this.sqlBuilder);
    }

    public OrderBy<T> orderBy(Result result) {
        return new OrderBy<>(this.sqlBuilder, result);
    }

    public GroupBy<T> groupBy(Result result) {
        return new GroupBy<>(this.sqlBuilder, result);
    }

    public Limit<T> limit(Integer count) {
        return new Limit<>(this.sqlBuilder, count);
    }

    public Limit<T> limit(Integer offset, Integer count) {
        return new Limit<>(this.sqlBuilder, offset, count);
    }

}
