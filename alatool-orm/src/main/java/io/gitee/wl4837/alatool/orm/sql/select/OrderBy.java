package io.gitee.wl4837.alatool.orm.sql.select;

import io.gitee.wl4837.alatool.orm.sql.SqlBuilder;
import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;
import io.gitee.wl4837.alatool.orm.sql.column.handler.Result;

/**
 * @author kingood
 */
public class OrderBy<T> extends SqlBuilderItem {

    private final SqlBuilder sqlBuilder;

    public OrderBy(SqlBuilder sqlBuilder, Result result) {
        this.sqlBuilder = sqlBuilder;
    }

    public OrderByDesc<T> desc(){
        return new OrderByDesc<>(this.sqlBuilder);
    }

    public OrderByAsc<T> asc(){
        return new OrderByAsc<>(this.sqlBuilder);
    }

}
