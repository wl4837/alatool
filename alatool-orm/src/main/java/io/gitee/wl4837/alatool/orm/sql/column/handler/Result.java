package io.gitee.wl4837.alatool.orm.sql.column.handler;


/**
 * 结果处理
 *
 * @author kingood
 */
public interface Result {

    default String toSql() {
        return "";
    }

}
