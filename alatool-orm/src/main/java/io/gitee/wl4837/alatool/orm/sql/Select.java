package io.gitee.wl4837.alatool.orm.sql;

import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;
import io.gitee.wl4837.alatool.orm.sql.column.handler.Result;
import io.gitee.wl4837.alatool.orm.sql.select.Form;
import io.gitee.wl4837.alatool.orm.sql.select.Where;

/**
 * @author kingood
 */
public class Select extends SqlBuilderItem {

    public Select(SqlBuilder sqlBuilder, Result[] results) {
        this.sqlBuilder = sqlBuilder;
        this.sqlBuilder.appendSqlLink("select");
        for (Result result : results) {
            this.sqlBuilder.appendSqlLink(result.toSql());
        }
    }

    public <T> Form<T> form(Class<T> table) {
        return new Form<>(this.sqlBuilder, table);
    }

    public <T> Where<T> where() {
        return new Where<>(this.sqlBuilder);
    }

}
