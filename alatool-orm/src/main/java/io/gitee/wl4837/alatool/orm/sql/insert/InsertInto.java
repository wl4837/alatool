package io.gitee.wl4837.alatool.orm.sql.insert;

import io.gitee.wl4837.alatool.orm.sql.SqlBuilder;
import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;

import java.util.List;

/**
 * @author kingood
 */
public class InsertInto<T> extends SqlBuilderItem {

    private final SqlBuilder sqlBuilder;

    public InsertInto(SqlBuilder sqlBuilder, Class<T> table) {
        this.sqlBuilder = sqlBuilder;
    }

    public Value<T> value(T data) {
        return new Value<>(this.sqlBuilder, data);
    }

    public Values<T> values(List<T> list) {
        return new Values<>(this.sqlBuilder, list);
    }

    @SafeVarargs
    public final Values<T> values(T... list) {
        return new Values<>(this.sqlBuilder, list);
    }

}
