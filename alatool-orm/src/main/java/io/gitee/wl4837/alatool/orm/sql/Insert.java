package io.gitee.wl4837.alatool.orm.sql;

import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;
import io.gitee.wl4837.alatool.orm.sql.insert.InsertInto;

/**
 * @author kingood
 */
public class Insert extends SqlBuilderItem {

    public Insert(SqlBuilder sqlBuilder) {
        this.sqlBuilder = sqlBuilder;
    }

    public <T> InsertInto<T> into(Class<T> table) {
        return new InsertInto<>(this.sqlBuilder, table);
    }

}
