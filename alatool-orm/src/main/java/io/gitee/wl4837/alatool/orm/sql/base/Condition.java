package io.gitee.wl4837.alatool.orm.sql.base;

/**
 * @author kingood
 */

public enum Condition {

	eq("=", "购买协议"),
	like("like", "课程转换协议");

	private Condition(String code, String name) {
		this.code = code;
		this.name = name;
	}

	private String code;

	private String name;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
