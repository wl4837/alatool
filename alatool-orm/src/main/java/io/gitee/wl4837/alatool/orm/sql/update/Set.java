package io.gitee.wl4837.alatool.orm.sql.update;

import io.gitee.wl4837.alatool.orm.sql.SqlBuilder;
import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;
import io.gitee.wl4837.alatool.orm.sql.update.handler.Result;

/**
 * @author kingood
 */
public class Set<T> extends SqlBuilderItem implements Result<T> {

    private final SqlBuilder sqlBuilder;

    public Set(SqlBuilder sqlBuilder) {
        this.sqlBuilder = sqlBuilder;
    }

    public Where<T> where() {
        return new Where<>(this.sqlBuilder);
    }

}
