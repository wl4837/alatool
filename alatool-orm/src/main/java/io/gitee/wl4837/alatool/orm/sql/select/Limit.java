package io.gitee.wl4837.alatool.orm.sql.select;

import io.gitee.wl4837.alatool.orm.sql.SqlBuilder;
import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;
import io.gitee.wl4837.alatool.orm.sql.select.handler.Result;

/**
 * @author kingood
 */
public class Limit<T> extends SqlBuilderItem implements Result<T> {

    private final SqlBuilder sqlBuilder;

    public Limit(SqlBuilder sqlBuilder, Integer count) {
        this.sqlBuilder = sqlBuilder;
    }

    public Limit(SqlBuilder sqlBuilder, Integer offset, Integer count) {
        this.sqlBuilder = sqlBuilder;
    }

}
