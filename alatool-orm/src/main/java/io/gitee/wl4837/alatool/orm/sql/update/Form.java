package io.gitee.wl4837.alatool.orm.sql.update;


import io.gitee.wl4837.alatool.orm.sql.SqlBuilder;
import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;

/**
 * @author kingood
 */
public class Form<T> extends SqlBuilderItem {

    private final SqlBuilder sqlBuilder;

    public Form(SqlBuilder sqlBuilder, Class<?> table) {
        this.sqlBuilder = sqlBuilder;
    }

    public Where<T> where() {
        return new Where<>(this.sqlBuilder);
    }

    public Limit<T> limit(Integer count) {
        return new Limit<>(this.sqlBuilder, count);
    }

    public Limit<T> limit(Integer offset, Integer count) {
        return new Limit<>(this.sqlBuilder, offset, count);
    }

}
