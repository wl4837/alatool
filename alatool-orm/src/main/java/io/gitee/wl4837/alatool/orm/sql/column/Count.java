package io.gitee.wl4837.alatool.orm.sql.column;

import io.gitee.wl4837.alatool.core.getter.MethodBaseFieldGetter;
import io.gitee.wl4837.alatool.orm.sql.SqlBuilder;
import io.gitee.wl4837.alatool.orm.sql.column.handler.Result;

/**
 * @author kingood
 */
public class Count extends SqlBuilder implements Result {

    public Count(SqlBuilder selectItem) {

    }

    public <P, R> Count(SqlBuilder selectItem, MethodBaseFieldGetter<P,R> methodBaseFieldGetter) {

    }

}
