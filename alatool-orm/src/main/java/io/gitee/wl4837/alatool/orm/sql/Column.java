package io.gitee.wl4837.alatool.orm.sql;

import io.gitee.wl4837.alatool.core.getter.MethodBaseFieldGetter;
import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;
import io.gitee.wl4837.alatool.orm.sql.column.As;

public class Column extends SqlBuilderItem {

    protected String prefix;

    public <T> Column(SqlBuilder selectItem, io.gitee.wl4837.alatool.orm.sql.select.handler.Result<T> result) {
//        this.sqlBuilder.appendSqlLink("(" + result.toSql() + ")");
    }

    public <R, P> Column(SqlBuilder selectItem, MethodBaseFieldGetter<P, R> methodBaseFieldGetter) {
//        this.sqlBuilder.appendSqlLink(methodBaseFieldGetter.getFieldName());
    }

    public <R, P> Column(SqlBuilder selectItem, String prefix, MethodBaseFieldGetter<P, R> methodBaseFieldGetter) {
        this.prefix = prefix;
//        this.sqlBuilder.appendSqlLink(prefix + methodBaseFieldGetter.getFieldName());
    }

    public <P, R> As as(MethodBaseFieldGetter<P, R> methodBaseFieldGetter) {
//        return new As(this.SqlBuilder, methodBaseFieldGetter);
        return null;
    }

}
