package io.gitee.wl4837.alatool.orm.sql.select;

import io.gitee.wl4837.alatool.orm.sql.SqlBuilder;
import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;
import io.gitee.wl4837.alatool.orm.sql.column.handler.Result;

/**
 * @author kingood
 */
public class GroupBy<T> extends SqlBuilderItem {

    private final SqlBuilder sqlBuilder;

    public GroupBy(SqlBuilder sqlBuilder, Result result) {
        this.sqlBuilder = sqlBuilder;
    }

    public OrderBy<T> orderBy(Result result) {
        return new OrderBy<>(this.sqlBuilder, result);
    }

    public Limit<T> limit(Integer count) {
        return new Limit<>(this.sqlBuilder, count);
    }

    public Limit<T> limit(Integer offset, Integer count) {
        return new Limit<>(this.sqlBuilder, offset, count);
    }

}
