package io.gitee.wl4837.alatool.orm.sql;

import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;
import io.gitee.wl4837.alatool.orm.sql.update.Set;

/**
 * @author kingood
 */
public class Update<T> extends SqlBuilderItem {

    public Update(SqlBuilder sqlBuilder, Class<T> table) {
        this.sqlBuilder = sqlBuilder;
    }

    public Set<T> set(T data) {
        return new Set<>(this.sqlBuilder);
    }

}
