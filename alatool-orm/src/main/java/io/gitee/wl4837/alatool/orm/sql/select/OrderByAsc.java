package io.gitee.wl4837.alatool.orm.sql.select;

import io.gitee.wl4837.alatool.orm.sql.SqlBuilder;
import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;
import io.gitee.wl4837.alatool.orm.sql.select.handler.Result;

/**
 * @author kingood
 */
public class OrderByAsc<T> extends SqlBuilderItem implements Result<T> {

    private final SqlBuilder sqlBuilder;

    public OrderByAsc(SqlBuilder sqlBuilder) {
        this.sqlBuilder = sqlBuilder;
    }

    public Limit<T> limit(Integer count) {
        return new Limit<>(this.sqlBuilder, count);
    }

    public Limit<T> limit(Integer offset, Integer count) {
        return new Limit<>(this.sqlBuilder, offset, count);
    }

}
