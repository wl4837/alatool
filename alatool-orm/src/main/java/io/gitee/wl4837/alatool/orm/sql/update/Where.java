package io.gitee.wl4837.alatool.orm.sql.update;

import io.gitee.wl4837.alatool.orm.sql.SqlBuilder;
import io.gitee.wl4837.alatool.orm.sql.base.Condition;
import io.gitee.wl4837.alatool.orm.sql.base.InFunc;
import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;
import io.gitee.wl4837.alatool.orm.sql.column.handler.Result;

/**
 * @author kingood
 */
public class Where<T> extends SqlBuilderItem {
    private final SqlBuilder sqlBuilder;

    public Where(SqlBuilder sqlBuilder) {
        this.sqlBuilder = sqlBuilder;
    }

    public WhereOn<T> on(Result result1, Result result2) {
        return new WhereOn<>(this.sqlBuilder, result1, result2);
    }

    public WhereOn<T> on(Result result1, Condition condition, Result result2) {
        return new WhereOn<>(this.sqlBuilder, result1, condition, result2);
    }

    public WhereIn<T> in(InFunc<Where<T>, WhereOn<T>> where) {
        return new WhereIn<>(this.sqlBuilder, where);
    }

}
