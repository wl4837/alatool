package io.gitee.wl4837.alatool.orm;

import io.gitee.wl4837.alatool.orm.sql.Select;
import io.gitee.wl4837.alatool.orm.sql.SqlBuilder;
import io.gitee.wl4837.alatool.orm.sql.Update;
import io.gitee.wl4837.alatool.orm.sql.Insert;
import io.gitee.wl4837.alatool.orm.sql.column.handler.Result;

import javax.sql.DataSource;
import java.io.Serializable;

/**
 * @author kingood
 */
public class Db extends AbstractDb implements Serializable {

    private static final long serialVersionUID = -3378415769645309514L;

    public Db(DataSource dataSource) {

    }

    public static Db use() {
        return use(null);
    }

    public static Db use(DataSource ds) {
        if (ds == null) {
            return new Db(null);
        } else {
            return new Db(ds);
        }
    }

    public static Db sql() {
        return null;
    }

    public Select select(Result... results) {
        return SqlBuilder.select(results);
    }

    public Insert insert() {
        return SqlBuilder.insert();
    }

    public <T> Update<T> update(Class<T> table) {
        return SqlBuilder.update(table);
    }

}
