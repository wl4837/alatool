package io.gitee.wl4837.alatool.orm.sql.select;

import io.gitee.wl4837.alatool.orm.sql.SqlBuilder;
import io.gitee.wl4837.alatool.orm.sql.base.Condition;
import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;
import io.gitee.wl4837.alatool.orm.sql.column.handler.Result;

/**
 * @author kingood
 */
public class LeftJoin<T> extends SqlBuilderItem {

    private final SqlBuilder sqlBuilder;

    public LeftJoin(SqlBuilder sqlBuilder, Class<?> table) {
        this.sqlBuilder = sqlBuilder;
    }

    public LeftJoinAs<T> as(String name) {
        return new LeftJoinAs<>(this.sqlBuilder, name);
    }

    public LeftJoinOn<T> on(Result result1, Result result2) {
        return new LeftJoinOn<>(this.sqlBuilder, result1, result2);
    }

    public LeftJoinOn<T> on(Result result1, Condition condition, Result result2) {
        return new LeftJoinOn<>(this.sqlBuilder, result1, condition, result2);
    }

}
