package io.gitee.wl4837.alatool.orm.sql.insert;

import io.gitee.wl4837.alatool.orm.sql.SqlBuilder;
import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;
import io.gitee.wl4837.alatool.orm.sql.insert.handler.Result;

/**
 * @author kingood
 */
public class Value<T> extends SqlBuilderItem implements Result<T> {

    private final SqlBuilder sqlBuilder;

    public Value(SqlBuilder sqlBuilder, T data) {
        this.sqlBuilder = sqlBuilder;
    }

}
