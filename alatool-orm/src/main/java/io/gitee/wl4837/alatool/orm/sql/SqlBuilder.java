package io.gitee.wl4837.alatool.orm.sql;

import io.gitee.wl4837.alatool.core.getter.MethodBaseFieldGetter;
import io.gitee.wl4837.alatool.orm.sql.base.Pre;
import io.gitee.wl4837.alatool.orm.sql.column.*;
import io.gitee.wl4837.alatool.orm.sql.column.handler.Result;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kingood
 */
public class SqlBuilder implements Serializable {

    private static final long serialVersionUID = -3378415769645309514L;

    public List<String> sqlLink = new ArrayList<>();

    public List<String> getSqlLink() {
        return sqlLink;
    }

    public void setSqlLink(List<String> sqlLink) {
        this.sqlLink = sqlLink;
    }

    public void appendSqlLink(String sqlLink){
        this.sqlLink.add(sqlLink);
    }

    public static Select select(Result... results) {
        return new Select(new SqlBuilder(), results);
    }

    public static Insert insert() {
        return new Insert(new SqlBuilder());
    }

    public static <T> Update<T> update(Class<T> table) {
        return new Update<>(new SqlBuilder(), table);
    }

    public static Delete delete() {
        return new Delete(new SqlBuilder());
    }

    public static Pre pre(String name) {
        return new Pre(new SqlBuilder(), name);
    }

    public static <P, R> Column col(MethodBaseFieldGetter<P, R> methodBaseFieldGetter) {
        return new Column(new SqlBuilder(), methodBaseFieldGetter);
    }

    public static <T> Column col(io.gitee.wl4837.alatool.orm.sql.select.handler.Result<T> result) {
        return new Column(new SqlBuilder(), result);
    }

    public static <P, R> Count count() {
        return new Count(new SqlBuilder());
    }

    public static <P, R> Count count(MethodBaseFieldGetter<P, R> methodBaseFieldGetter) {
        return new Count(new SqlBuilder(), methodBaseFieldGetter);
    }

    public static <P, R> Sum sum(Result result1, Result result2) {
        return new Sum(new SqlBuilder(), result1, result2);
    }

}
