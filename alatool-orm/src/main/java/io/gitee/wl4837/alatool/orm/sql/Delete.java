package io.gitee.wl4837.alatool.orm.sql;

import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;

/**
 * @author kingood
 */
public class Delete extends SqlBuilderItem {

    public Delete(SqlBuilder sqlBuilder) {
        this.sqlBuilder = sqlBuilder;
    }

}
