package io.gitee.wl4837.alatool.orm;

/**
 * @author Kingood
 */
public class User {

    public String name;

    private String phone;

    private Integer age;

    private Boolean sex;

    private String desc;

    public String getName() {
        return name + "W";
    }

    public String getPhone() {
        return phone;
    }

    public Integer getAge() {
        return age;
    }

    public Boolean getSex() {
        return sex;
    }

    public String getDesc() {
        return desc;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
