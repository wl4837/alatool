package io.gitee.wl4837.alatool.orm.sql.insert;

import io.gitee.wl4837.alatool.orm.sql.SqlBuilder;
import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;
import io.gitee.wl4837.alatool.orm.sql.insert.handler.Result;

import java.util.List;

/**
 * @author kingood
 */
public class Values<T> extends SqlBuilderItem implements Result<T> {

    private final SqlBuilder sqlBuilder;

    public Values(SqlBuilder sqlBuilder, T[] list) {
        this.sqlBuilder = sqlBuilder;
    }

    public Values(SqlBuilder sqlBuilder, List<T> list) {
        this.sqlBuilder = sqlBuilder;
    }

}
