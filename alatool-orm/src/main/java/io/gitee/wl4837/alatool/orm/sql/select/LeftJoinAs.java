package io.gitee.wl4837.alatool.orm.sql.select;

import io.gitee.wl4837.alatool.orm.sql.SqlBuilder;
import io.gitee.wl4837.alatool.orm.sql.base.Condition;
import io.gitee.wl4837.alatool.orm.sql.base.SqlBuilderItem;
import io.gitee.wl4837.alatool.orm.sql.column.handler.Result;

/**
 * @author kingood
 */
public class LeftJoinAs<T> extends SqlBuilderItem {

    private final SqlBuilder sqlBuilder;

    public LeftJoinAs(SqlBuilder sqlBuilder, String name) {
        this.sqlBuilder = sqlBuilder;
    }

    public LeftJoinOn<T> on(Result result1, Result result2) {
        return new LeftJoinOn<>(this.sqlBuilder, result1, result2);
    }

    public LeftJoinOn<T> on(Result result1, Condition condition, Result result2) {
        return new LeftJoinOn<>(this.sqlBuilder, result1, condition, result2);
    }

}
