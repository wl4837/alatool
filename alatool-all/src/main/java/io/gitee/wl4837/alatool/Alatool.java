package io.gitee.wl4837.alatool;

import io.gitee.wl4837.alatool.core.util.ClassUtil;

import java.util.Set;

public class Alatool {

	public static final String AUTHOR = "wl4837";

	private Alatool() {
	}

	/**
	 * 显示Alatool所有的工具类
	 *
	 * @return 工具类名集合
	 * @since 5.5.2
	 */
	public static Set<Class<?>> getAllUtils() {
//		return ClassUtil.scanPackage("cn.hutool",
//				(clazz) -> (false == clazz.isInterface()) && StrUtil.endWith(clazz.getSimpleName(), "Util"));
		return null;
	}

	/**
	 * 控制台打印所有工具类
	 */
	public static void printAllUtils() {
//		final Set<Class<?>> allUtils = getAllUtils();
//		final ConsoleTable consoleTable = ConsoleTable.create().addHeader("工具类名", "所在包");
//		for (Class<?> clazz : allUtils) {
//			consoleTable.addBody(clazz.getSimpleName(), clazz.getPackage().getName());
//		}
//		consoleTable.print();
	}
}
