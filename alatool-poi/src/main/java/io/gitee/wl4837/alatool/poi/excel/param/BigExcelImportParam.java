package io.gitee.wl4837.alatool.poi.excel.param;

import java.io.Serializable;

/**
 * 大数据导入参数
 * @param <T>
 * @author wangliang
 */
public class BigExcelImportParam<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 导出备注
     */
    private String remarks;

    /**
     * 导出备注
     */
    private T param;

    /**
     * 分页数量
     */
    public Integer pageSize = 3000;

    /**
     * 异步导出
     */
    private Boolean async = true;

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public T getParam() {
        return param;
    }

    public void setParam(T param) {
        this.param = param;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Boolean getAsync() {
        return async;
    }

    public void setAsync(Boolean async) {
        this.async = async;
    }

}
