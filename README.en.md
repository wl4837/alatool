<h2 align="center">AlaTool Tool Framework</h2>

<p align="center">
<a href="https://gitee.com/wl4837/alatool/stargazers"><img src="https://gitee.com/wl4837/alatool/badge/star.svg?theme=dark" alt="Star"></a>
<a href="https://gitee.com/wl4837/alatool/members"><img src="https://gitee.com/wl4837/alatool/badge/fork.svg?theme=dark" alt="Fork"></a>
</p>

[中文](./README.md) |
English

#### Description
🐶 Java Tool class library to develop projects more quickly, use class library conveniently and elegantly.

#### Functional Module

|  Module | Description |
|---|-------------|
|  alatool-core | core        |
|  alatool-orm | database    |

#### Installation

### Maven
```
<dependency>
    <groupId>io.gitee.wl4837.alatool</groupId>
    <artifactId>alatool-all</artifactId>
    <version>0.0.6</version>
</dependency>
```
## Use documents

1.  [Form Validate Util](./alatool-core/src/test/java/io/gitee/wl4837/alatool/core/util/FormValidateUtilTest.java)
2.  [Tree Util](./alatool-core/src/test/java/io/gitee/wl4837/alatool/core/util/TreeUtilTest.java)

#### Gitee Feature

1.  Fork Ben warehouse
2.  Create Feat_xxx branch
3.  Submit code
4.  Create Pull Request
