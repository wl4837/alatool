<h2 align="center">AlaTool 工具框架</h2>

<p align="center">
<a href="https://gitee.com/wl4837/alatool/stargazers"><img src="https://gitee.com/wl4837/alatool/badge/star.svg?theme=dark" alt="Star"></a>
<a href="https://gitee.com/wl4837/alatool/members"><img src="https://gitee.com/wl4837/alatool/badge/fork.svg?theme=dark" alt="Fork"></a>
</p>

中文 | 
[English](./README.en.md)

## 简介
🐶 Java工具类库 更加快速的开发项目 便捷优雅的使用类库

## 功能模块

|  模块 |  介绍 |
|---|---|
|  alatool-core |  核心 |
|  alatool-orm | 数据库  |


## 安装

### Maven
```
<dependency>
    <groupId>io.gitee.wl4837.alatool</groupId>
    <artifactId>alatool-all</artifactId>
    <version>0.0.6</version>
</dependency>
```

## 使用文档

1.  [表单数据验证工具](./alatool-core/src/test/java/io/gitee/wl4837/alatool/core/util/FormValidateUtilTest.java)
2.  [树结构工具](./alatool-core/src/test/java/io/gitee/wl4837/alatool/core/util/TreeUtilTest.java)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request